@extends('emails.v1.layout')
@section('content')
@component('emails.v1.components.paragraph')
@slot('rowHeading')
{{ Lang::get('email.forgotPassword.heading') }}
@endslot
@slot('paragraphHeading')
{{ Lang::get('email.forgotPassword.paragraphHeading')  . $userName }}
@endslot
@slot('paragraph')
{{ Lang::get('email.forgotPassword.paragraph') }}
@endslot
@slot('actionUrl')
{{ $resetPasswordLink }}
@endslot
@slot('actionText')
{{ Lang::get('email.forgotPassword.actionText') }}
@endslot
@slot('closingParagraphHeading')
{{ Lang::get('email.forgotPassword.closingParagraphHeading') }}
@endslot
@slot('closingParagraph')
{{ Lang::get('email.forgotPassword.closingParagraph') }}
@component('emails.v1.components.partials.anchorTag')
@slot('anchorLink')
{{ $resetPasswordLink }}
@endslot
@slot('anchorText')
{{ $resetPasswordLink }}
@endslot
@endcomponent
@endslot
@endcomponent
@endsection

