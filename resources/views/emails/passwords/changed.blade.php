@extends('emails.v1.layout')
@section('content')
@component('emails.v1.components.paragraph')
@slot('rowHeading')
{{ Lang::get('email.changePassword.heading') }}
@endslot
@slot('paragraphHeading')
{{ Lang::get('email.changePassword.paragraphHeading')  . $userName }}
@endslot
@slot('paragraph')
{{ Lang::get('email.changePassword.paragraph') }}
@endslot
@slot('actionUrl')
{{ $resetPasswordLink }}
@endslot
@slot('actionText')
{{ Lang::get('email.changePassword.actionText') }}
@endslot
@slot('closingParagraphHeading')
{{ Lang::get('email.changePassword.closingParagraphHeading') }}
@endslot
@slot('closingParagraph')
{{ Lang::get('email.changePassword.closingParagraph') }}
@component('emails.v1.components.partials.anchorTag')
@slot('anchorLink')
{{ $resetPasswordLink }}
@endslot
@slot('anchorText')
{{ $resetPasswordLink }}
@endslot
@endcomponent
@endslot
@endcomponent
@endsection
