<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width" initial-scale="1.0" />
    <title>Hyve Email</title>

    <style type="text/css">
        /* Reset Styles*/
        body, #bodyTable, #bodyCell {
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            width: 100% !important;
        }
        table {
            border-collapse: collapse;
        }
        img, a img {
            border: 0;
            outline: 0;
            text-decoration: none;
        }
        h1, h2, h3, h4, h5, h6 {
            margin: 0;
            padding: 0;
        }

        /* Client Specific styles */

        /*
            Force Hotmail/Outlook to display emails at full width
        */
        .ReadMsgBody{
            width: 100%;
        }
        .ExternalClass {
            width: 100%;
        }

        /* Force Hotmail/Outlook to display line-heights normally */
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
            line-height: 100%;
        }

        /* Remove spacing between tables in Outlook 2007 and up. */
        table, td, th {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        th {
            margin: 0 !important;
        }

        /* Force Outlook 2007 and up to provide a "view in browser message" */
        #outlook a {
            padding: 0;
        }

        /* Force IE to smoothly render resized images */
        img {
            -ms-interpolation-mode: bicubic;
        }

        /* Prevent Windows and Webkit based mobile platforms from changing declared text sizes */
        body, table, td, p, a, li, blockquote, th {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        /* General styles */
        h1, h2, h3 {
            font-family: "HelveticaNeue-Medium", Helvetica, Arial, sans-serif;
        }

        h1, h1 a {
            color: #FFFFFF;
            font-size: 16px;
            font-weight: 300;
            line-height: 125%;
            text-align: left;
            text-transform: uppercase;
            letter-spacing: 3.2px;
        }

        h2, h2 a {
            color: #384252;
            font-size: 15px;
            font-weight: 300;
            line-height: 133%;
            text-align: left;
            letter-spacing: 3.2px;
            text-transform: uppercase;
        }

        h3, h3 a {
            color: #485465;
            font-size: 14px;
            font-weight: 600;
            line-height: 128%;
            text-align: left;
            text-transform: uppercase;
            letter-spacing: 1.8px;
        }

        p {
            text-align: left;
            font-size: 11px;
            color: #727881;
            line-height: 22px;
            text-transform: none;
        }

        p a {
            text-decoration: none;
            color: #e6442c;
            font-size: 11px;
            word-wrap: break-word;
        }

        .button {
            box-sizing: border-box;
            color: #FFF;
            text-transform: uppercase;
            display: inline-block;
            text-decoration: none;
            -webkit-text-size-adjust: none;
            width: 250px;
            height: 50px;
            background-color: #ed5b39;
            line-height: 50px;
            font-size: 15px;
        }

        .giant-text {
            width: 222px;
            height: 48px;
            font-family: HelveticaNeue;
            font-size: 39.6px;
            font-weight: bold;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: 1px;
            text-align: center;
            color: #ed5b39;
        }

        .tableHeader {
            font-size: 10px;
            letter-spacing: 1.4px;
            color: #ed5b39;
            background: #FFFFFF;
            padding-top: 14px;
            padding-right: 8px;
            padding-bottom: 16px;
            padding-left: 8px;
            font-family: "HelveticaNeue-Medium", Helvetica, Arial, sans-serif;
            font-weight: 300;
            text-transform: uppercase;
        }

        .tableText {
            font-size: 10px;
            letter-spacing: 1px;
            color: #ed5b39;
            background: #FFFFFF;
            padding-top: 40px;
            padding-right: 40px;
            padding-bottom: 40px;
            padding-left: 40px;
            font-family: "HelveticaNeue-Medium", Helvetica, Arial, sans-serif;
            font-weight: 300;
            text-transform: uppercase;
        }

        .tableCell {
            font-size: 12px;
            letter-spacing: 1.2px;
            color: #727881;
            font-weight: 400;
            padding-top: 11px;
            padding-right: 8px;
            padding-bottom: 14px;
            padding-left: 8px;
            background: #FFFFFF;
            border-top: 2px solid #4A3C44;
            font-family: "SourceSansPro-Regular", Helvetica, Arial, sans-serif;
            text-transform: uppercase;
        }

        .borderHighlight {
            border-left: 2px solid #f2f5f7;
        }

        .noTopBorder {
            border-top: none;
        }

        .tableBorderTop {
            border-top: 2px solid #f2f5f7;
        }

        .boldText {
            font-weight: 600;
        }

        .dividerCell {
            padding-top: 7px;
            padding-bottom: 7px;
            padding-left: 40px;
        }

        .divider {
            background: #f15a31;
            width: 15px;
            height: 3px;
            display: block;
        }

        .dividerGrey {
            background: #f15a31;
            width: 15px;
            height: 3px;
            display: block;
        }

        .sectionHeaderWrapper {
            padding-top: 16px;
            padding-bottom: 17px;
        }

        /* Each of the tables wrappers */
        .blockWrapper {
            padding-right: 20px;
            padding-bottom: 20px;
            padding-left: 20px;
            background: #f2f5f7;
        }

        .fixedLayout {
            table-layout: fixed;
        }

        .white-bg {
            width: 270px;
            height: 125px;
            background-color: #ffffff;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            border: 2px solid #fafafa;
        }

        /* This class needs to be applied to dates, so that the blue, and underline doesnt occur on apple devices */
        /*a[href^="x-apple-data-detectors:"] {
            color: inherit;
            text-decoration: inherit;
        }*/
        /* **************** End of General Styles  **************** */


        /* Body Styles */
        body, #bodyTable {
            background-color: #FFFFFF;
        }
        /* **************** End of Body Styles  **************** */

        /* Email Container Styles */
        #emailContainer {
            width: 100%;
        }
        /* **************** End of Email Container Styles  **************** */


        /* Footer Styles */
        #footerCell {
            padding-top: 20px;
            padding-bottom: 20px;
        }
        /* **************** End of Footer Styles  **************** */

    </style>
</head>

<body>
<center>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        <tr>
            <td align="center" valign="top" id="bodyCell">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="emailContainer">

                    @include('emails.v1.includes.header')

                    @yield('content')

                    @include('emails.v1.includes.footer')

                </table>
            </td>
        </tr>
    </table>
</center>
</body>
</html>