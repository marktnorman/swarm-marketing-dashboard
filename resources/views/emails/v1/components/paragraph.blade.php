<!-- Text block -->
<tr>
    <th align="center" valign="top">
        <table border="0" cellpadding="0" cellspacing="0" width="720">
            <tr>
                <th align="center" valign="top" class="blockWrapper">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="center" valign="top">
                                <!-- Header and date -->
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="left" valign="top" class="sectionHeaderWrapper">
                                            <h3>{{$rowHeading}}</h3>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="fixedLayout">
                                    @if(isset($paragraph))
                                        <tr>
                                            <th align="center" class="tableText">
                                                @if(isset($paragraphHeading))
                                                    <h3>{{$paragraphHeading}}</h3>
                                                @endif
                                                <p>{{$paragraph}}</p>
                                            </th>
                                        </tr>
                                    @endif
                                    @if(isset($actionUrl) && isset($actionText))
                                        <tr>
                                            <th align="center" valign="middle" class="tableText">
                                                <a href="{{$actionUrl}}" class="button">{{$actionText}}</a>
                                            </th>
                                        </tr>
                                    @endif
                                    @if(isset($closingParagraph))
                                        <tr>
                                            <th align="center" class="tableText">
                                                @if(isset($closingParagraphHeading))
                                                    <h3>{{$closingParagraphHeading}}</h3>
                                                @endif
                                                <p>{{$closingParagraph}}</p>
                                            </th>
                                        </tr>
                                    @endif
                                </table>
                            </td>
                        </tr>
                    </table>
                </th>
            </tr>
        </table>
    </th>
</tr>
<!-- End of text block -->