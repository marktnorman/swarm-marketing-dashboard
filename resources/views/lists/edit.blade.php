@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Manage Bind</div>

                    <div class="panel-body">
                        <!-- if there are creation errors, they will show here -->
                        {{ Html::ul($errors->all()) }}

                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif

                        {!! Form::model($list, ['route' => ['lists.update', $list->id], 'method' => 'PUT']) !!}
                        {!! Form::token() !!}

                        <div class="form-group">
                            {!! Form::label('list_option', 'Opt in list item') !!}
                            {!! Form::text('list_option', $list->list, ['class' => 'form-control']) !!}
                        </div>

                        {!! Form::submit('Update', array('class' => 'btn btn-primary')) !!}
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
