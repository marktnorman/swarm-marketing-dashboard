@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Create List item</div>
                    <div class="panel-body">
                        <!-- if there are creation errors, they will show here -->
                        {{ Html::ul($errors->all()) }}

                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif

                        {{ Form::open(['url' => 'lists', 'method' => 'POST']) }}

                        <div class='form-group'>
                            {!! Form::label('list_option', 'Opt in list item:') !!}
                            {!! Form::text('list_option', null, ['class' => 'form-control']) !!}
                        </div>

                        {{ Form::submit('Create list item', ['class' => 'btn btn-primary']) }}

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
