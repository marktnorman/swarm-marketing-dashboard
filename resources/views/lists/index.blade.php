@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Lists</div>

                    <div class="panel-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($lists as $list) { ?>
                                <tr>
                                    <td class="col-md-4"><?php echo $list->list; ?></td>
                                    <td class="col-md-4"><a href="{{ url('/lists/' . $list->id . '/edit') }}">Edit</a></td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                        <a class="create-button" href="{{ url('/lists/create') }}">Upload List</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
