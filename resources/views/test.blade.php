<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>
</head>
<body>
<h1>Notifications</h1>

<p>Notifications: @{{ notifications.length }}</p>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/0.12.16/vue.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />

<script>
    var socket = io('http://swarm.dev:6001');
    new Vue({
        el: 'body',
        data: {
            notifications: []
        },
        ready: function() {
            socket.on('private-notifications-channel.1:App\\Notifications', function(data) {
                console.log(data);
                toastr[data.notificationType](data.message);
                this.notifications.push(data.notification);
            }.bind(this));
        }
    });
</script>
</body>
</html>