@extends('layouts.app')

@section('datepicker-dependencies-styles')
    <!-- Datepicker for send date on campaign creation -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Manage Campaign</div>
                    <div class="panel-body">

                        <div class="general-actions">
                            <div class="back-button">
                                <a class="btn btn-primary" href="{{ url()->previous() }}">&#8592; Back</a>
                            </div>
                            <!-- Delete button -->
                            {{ Form::model($campaign, ['route' => ['campaigns.destroy', $campaign->id], 'method' => 'DELETE']) }}
                                {{ Form::submit('Delete campaign', array('class' => 'btn btn-danger button-right')) }}
                            {{ Form::close() }}
                        </div>

                        <!-- if there are creation errors, they will show here -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif

                        {{ Form::model($campaign, ['route' => ['campaigns.update', $campaign->id], 'method' => 'PUT']) }}

                        {{ Form::token() }}

                        <div class="form-group">
                            {{ Form::label('campaign_title', 'Title') }}
                            {{ Form::text('campaign_title', $campaign->title, ['name' => 'title', 'class' => 'form-control']) }}
                            {{ $errors->first('campaign_title', '<p class="help-block">:message</p>') }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('campaign_copy', 'Copy') }}
                            {{ Form::textarea('campaign_copy', $campaign->copy, ['name' => 'copy', 'class' => 'form-control']) }}
                            {{ $errors->first('campaign_copy', '<p class="help-block">:message</p>') }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('from', 'From number') }}
                            {{ Form::text('from', $campaign->from, ['name' => 'from', 'class' => 'form-control']) }}
                            {{ $errors->first('from', 'Please enter the from number when executing the campaign') }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('date', 'Send date') }}
                            {{ Form::text('send_date', $datestamp, array('id' => 'datepicker', 'class' => 'form-control')) }}
                            {{ $errors->first('date', '<p class="help-block">:message</p>') }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('campaign_time', 'Send time') }}
                            {{ Form::select('campaign_time', $times, $timestamp, array('class' => 'form-control')) }}
                            {{ $errors->first('campaign_time', 'Please select your time when you would like this campaign to run') }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('segment_id', 'Segment') }}
                            {{ Form::select('segment_id', $segments, $campaign->segment_id, array('class' => 'form-control')) }}
                            {{ $errors->first('segment_id', '<p class="help-block">:message</p>') }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('bind_id', 'Bind') }}
                            {{ Form::select('bind_id', $binds, $campaign->bind_id, array('class' => 'form-control')) }}
                            {{ $errors->first('bind_id', '<p class="help-block">:message</p>') }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('campaign_type_id', 'Campaign Type') }}
                            {{ Form::select('campaign_type_id', $campaign_type, $campaign->campaign_type, array('class' => 'form-control')) }}
                            {{ $errors->first('campaign_type_id', '<p class="help-block">:message</p>') }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('service_id', 'Service ID') }}
                            {{ Form::text('service_id', $campaign->service_id, ['name' => 'service_id', 'class' => 'form-control']) }}
                            {{ $errors->first('service_id', '<p class="help-block">:message</p>') }}
                        </div>

                        {{ Form::submit('Update campaign', array('class' => 'btn btn-primary')) }}

                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('datepicker-dependencies-scripts')
    <!-- Datepicker for send date on campaign creation -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#datepicker").datepicker({
                dateFormat: 'yy-mm-dd'
            });
        });
    </script>
@stop