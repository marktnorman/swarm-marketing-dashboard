@extends('layouts.app')

@section('datepicker-dependencies-styles')
    <!-- Datepicker for send date on campaign creation -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create campaign</div>

                    <div class="panel-body">

                        <!-- if there are creation errors, they will show here -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif

                        {{ Form::open(['route' => ['campaigns.store'], 'enctype' => 'multipart/form-data', 'method' => 'POST']) }}

                        {{ Form::token() }}

                        <div class="form-group">
                            {{ Form::label('campaign_title', 'Title') }}
                            {{ Form::text('campaign_title', null, ['name' => 'title', 'class' => 'form-control']) }}
                            {{ $errors->first('campaign_title', 'Please enter a campaign title') }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('campaign_copy', 'Copy') }}
                            {{ Form::textarea('campaign_copy', null, ['name' => 'copy', 'class' => 'form-control']) }}
                            {{ $errors->first('campaign_copy', 'Please enter what will be viewed in the sms and no longer than 160 characters') }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('from', 'From number') }}
                            {{ Form::text('from', null, ['name' => 'from', 'class' => 'form-control']) }}
                            {{ $errors->first('from', 'Please enter the from number when executing the campaign') }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('campaign_date', 'Send date') }}
                            {{ Form::text('send_date', '', array('id' => 'datepicker', 'class' => 'form-control')) }}
                            {{ $errors->first('campaign_date', 'Please select on which date you would like this campaign to run') }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('campaign_time', 'Send time') }}
                            {{ Form::select('campaign_time', $times, '00:00:00', array('class' => 'form-control')) }}
                            {{ $errors->first('campaign_time', 'Please select your time when you would like this campaign to run') }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('segment_id', 'Segment') }}
                            {{ Form::select('segment_id', $segments, '', array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('bind_id', 'Bind') }}
                            {{ Form::select('bind_id', $binds, '', array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('campaign_type_id', 'Campaign Type') }}
                            {{ Form::select('campaign_type_id', $campaign_type, '', array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('service_id', 'Service ID') }}
                            {{ Form::text('service_id', null, ['name' => 'service_id', 'class' => 'form-control']) }}
                            {{ $errors->first('service_id', 'Please enter your service ID') }}
                        </div>

                        {{ Form::submit('Create campaign', array('class' => 'btn btn-primary')) }}

                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('datepicker-dependencies-scripts')
    <!-- Datepicker for send date on campaign creation -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#datepicker").datepicker({
                dateFormat: 'yy-mm-dd'
            });
        });
    </script>
@stop
