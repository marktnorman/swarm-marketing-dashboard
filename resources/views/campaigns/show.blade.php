@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Manage {{ $name }}</div>
                    <div class="panel-body">
                        <div class="back-button">
                            <a href="{{ url()->previous() }}">&#8592; Back</a>
                        </div>
                        <p>General reporting stats of the {{ $name }} campaign</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection