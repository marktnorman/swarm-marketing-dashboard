@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Campaigns</div>

                    <div class="panel-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Copy</th>
                                <th>Send date</th>
                                <th>Progress</th>
                            </tr>
                            </thead>
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            <tbody>
                            @foreach($campaigns['data'] as $campaign)

                                <tr>
                                    <td class="col-md-3"><a href="{{ url('/campaigns/' . $campaign['id']) }}">{{ $campaign['attributes']['title'] }}</a>
                                    </td>
                                    <td class="col-md-3">{{ $campaign['attributes']['copy'] }}</td>
                                    <td class="col-md-4">{{ $campaign['attributes']['send_date'] }}</td>
                                    @if($campaign['attributes']['status'] == \App\Models\Campaign::EXECUTED)
                                        <td class="col-md-4">Executed</td>
                                    @elseif($campaign['attributes']['status'] == \App\Models\Campaign::CANCELLED)
                                        <td class="col-md-4">Cancelled</td>
                                    @elseif($campaign['attributes']['status'] == \App\Models\Campaign::SCHEDULED)
                                        <td class="col-md-4">Scheduled</td>
                                    @else
                                        <td class="col-md-4">Unknown State</td>
                                    @endif
                                    <td class="col-md-4"><a href="{{ url('/campaigns/' . $campaign['id'] . '/edit') }}">Edit</a></td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <a class="create-button" href="{{ url('/campaigns/create') }}">Create campaign</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
