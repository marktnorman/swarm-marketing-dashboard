@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Segments</div>
                    <div class="panel-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Type</th>
                                <th>Updated / Created at</th>
                                <th>Progress</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif

                            <tbody>
                            <?php
                            foreach ($segments['data'] as $segment) {

                            $status = $segment['attributes']['stored'];
                            switch ($status) {
                                case App\Models\Segments::UPLOADABLE:
                                    $status = '<span class="campaign-green">Scheduled</span>';
                                    break;
                                case App\Models\Segments::INPROGRESS:
                                    $status = '<span class="campaign-green">In progress</span>';
                                    break;
                                case App\Models\Segments::STORED:
                                    $status = '<span class="campaign-green">Stored</span>';
                                    break;
                                case App\Models\Segments::FAILED:
                                    $status = '<span class="campaign-red">Failed</span>';
                                    break;
                            }
                            ?>
                            <tr>
                                <td class="col-md-3"><?php echo $segment['attributes']['title'];?></td>

                                <td class="col-md-3"><?php echo $segment['attributes']['type'];?></td>
                                <td class="col-md-4"><?php echo $segment['attributes']['updated_at'];?></td>
                                <td class="col-md-4"><?php echo $status;?></td>

                                <td class="col-md-4"><a href="{{ url('/segments/' . $segment['id'] . '/edit') }}">Edit</a>
                                </td>

                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <a class="create-button" href="{{ url('/segments/create') }}">Upload Segment</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
