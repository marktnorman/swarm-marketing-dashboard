@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Manage Segment</div>

                    <div class="panel-body">

                        <!-- if there are creation errors, they will show here -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <!-- Success messages -->
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif

                        {{ Form::open(['url' => ['segments'], 'enctype' => 'multipart/form-data', 'method' => 'POST']) }}

                        {{ Form::token() }}

                        <div class="form-group">
                            {{ Form::label('title', 'Title') }}
                            {{ Form::text('title', null, ['name' => 'title', 'class' => 'form-control']) }}
                            {{ $errors->first('title', '<p class="help-block">:message</p>') }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('delimiter_type', 'Delimiter') }}
                            {{ Form::select('delimiter_type', $delimiters, '2', array('class' => 'form-control')) }}
                            {{ $errors->first('delimiter_type', 'Please select your desired delimiter') }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('file_name', 'CSV file') }}
                            {{ Form::file('file_name') }}
                        </div>

                        {{ Form::submit('Create segment', array('class' => 'btn btn-primary')) }}

                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
