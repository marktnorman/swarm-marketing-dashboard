@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Manage Segment</div>
                    <div class="panel-body">

                        <div class="general-actions">
                            <div class="back-button">
                                <a href="{{ url()->previous() }}">&#8592; Back</a>
                            </div>
                            <!-- Delete button -->
                            {{ Form::model($segment, ['route' => ['segments.destroy', $segment->id], 'method' => 'DELETE']) }}
                            {{ Form::submit('Delete segment', array('class' => 'btn btn-danger button-right')) }}
                            {{ Form::close() }}
                        </div>

                        <!-- if there are creation errors, they will show here -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                    <!-- Success messages -->
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif

                        {{ Form::model($segment, ['route' => ['segments.update', $segment->id], 'enctype' => 'multipart/form-data', 'method' => 'PUT']) }}

                        {{ Form::token() }}

                        <div class="form-group">
                            {{ Form::label('title', 'Title') }}
                            {{ Form::text('title', $segment->title, ['name' => 'title', 'class' => 'form-control']) }}
                            {{ $errors->first('title', '<p class="help-block">:message</p>') }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('delimiter_type', 'Delimiter') }}
                            {{ Form::select('delimiter_type', $delimiters, $segment->delimiter_type, array('class' => 'form-control')) }}
                            {{ $errors->first('delimiter_type', 'Please select your desired delimiter') }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('csv_file', 'CSV file') }}
                            <p>{{ $segment->file_name }}</p>
                        </div>

                        {{ Form::submit('Update segment', array('class' => 'btn btn-primary')) }}
                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
