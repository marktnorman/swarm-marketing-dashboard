@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Manage Bind</div>
                    <div class="panel-body">
                        <div class="bind-helper-text">
                            <p>
                                Swarm allows editors to create different binds which acts as channels and can be used
                                per campaign. eg. Vodacom, Cell C, Zamtel. Bind information can be found <a
                                        href="https://hyvemobile.atlassian.net/wiki/spaces/SDP/pages/4030499/TotalSend#TotalSend-TXAccounts"
                                        target="_blank">here</a>
                            </p>
                        </div>
                        <!-- if there are creation errors, they will show here -->
                        {{ Html::ul($errors->all()) }}

                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif

                        {!! Form::model($bind, ['route' => ['binds.update', $bind['data']['id']], 'method' => 'PUT']) !!}
                        {!! Form::token() !!}

                        <div class="form-group">
                            {!! Form::label('bind_title', 'Title') !!}
                            {!! Form::text('bind_title', $bind['data']['attributes']['bind_title'], ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('bind_username', 'Username') !!}
                            {!! Form::text('bind_username', $bind['data']['attributes']['config']['bind_username'], ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('bind_password', 'Password')!!}
                            {!! Form::text('bind_password', $bind['data']['attributes']['config']['bind_password'], ['class' => 'form-control']) !!}
                        </div>

                        <div class='form-group'>
                            {!! Form::label('api_endpoint_url', 'API endpoint URL:') !!}
                            {!! Form::text('api_endpoint_url', $bind['data']['attributes']['config']['api_endpoint_url'], ['class' => 'form-control']) !!}
                        </div>

                        {!! Form::submit('Update', array('class' => 'btn btn-primary')) !!}
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
