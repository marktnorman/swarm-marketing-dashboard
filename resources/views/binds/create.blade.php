@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Create Bind</div>
                    <div class="panel-body">
                        <div class="bind-helper-text">
                            <p>
                                Swarm allows editors to create different binds which acts as channels and can be used
                                per campaign. eg. Vodacom, Cell C, Zamtel. Bind information can be found <a
                                        href="https://hyvemobile.atlassian.net/wiki/spaces/SDP/pages/4030499/TotalSend#TotalSend-TXAccounts"
                                        target="_blank">here</a>
                            </p>
                        </div>
                        <!-- if there are creation errors, they will show here -->
                        {{ Html::ul($errors->all()) }}

                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif

                        {{ Form::open(['url' => 'binds', 'method' => 'POST']) }}

                        <div class='form-group'>
                            {!! Form::label('bind_title', 'Title:') !!}
                            {!! Form::text('bind_title', null, ['class' => 'form-control']) !!}
                        </div>

                        <div class='form-group'>
                            {!! Form::label('bind_username', 'Username:') !!}
                            {!! Form::text('bind_username', null, ['class' => 'form-control']) !!}
                        </div>

                        <div class='form-group'>
                            {!! Form::label('bind_password', 'Password:') !!}
                            {!! Form::password('bind_password', ['class' => 'form-control']) !!}
                        </div>

                        <div class='form-group'>
                            {!! Form::label('api_endpoint_url', 'API endpoint URL:') !!}
                            {!! Form::text('api_endpoint_url', null, ['class' => 'form-control']) !!}
                        </div>

                        {{ Form::submit('Create bind', ['class' => 'btn btn-primary']) }}

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
