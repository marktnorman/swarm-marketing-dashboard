@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Binds</div>

                    <div class="panel-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Config</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($binds['data'] as $bind)
                                <tr>
                                    <td class="col-md-4">{{ $bind['attributes']['bind_title'] }}</td>
                                    <td class="col-md-4"><a href="{{ url('/binds/' . $bind['id'] . '/edit') }}">Edit</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <a class="create-button" href="{{ url('/binds/create') }}">Upload Bind</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
