<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SegmentsRepository
 * @package namespace App\Interfaces;
 */
interface SegmentsRepository extends RepositoryInterface
{
    //
}
