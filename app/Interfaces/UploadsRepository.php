<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UploadsRepository
 * @package namespace App\Interfaces;
 */
interface UploadsRepository extends RepositoryInterface
{
    //
}
