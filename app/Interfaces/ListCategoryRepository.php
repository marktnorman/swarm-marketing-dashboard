<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ListCategoryRepository
 * @package namespace App\Interfaces;
 */
interface ListCategoryRepository extends RepositoryInterface
{
    //
}
