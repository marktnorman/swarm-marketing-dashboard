<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ChunkPartRepository
 * @package namespace App\Interfaces;
 */
interface ChunkPartRepository extends RepositoryInterface
{
    //
}
