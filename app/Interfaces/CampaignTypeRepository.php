<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CampaignTypeRepository
 * @package namespace App\Interfaces;
 */
interface CampaignTypeRepository extends RepositoryInterface
{
    //
}
