<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ChunkMasterRepository
 * @package namespace App\Interfaces;
 */
interface ChunkMasterRepository extends RepositoryInterface
{
    //
}
