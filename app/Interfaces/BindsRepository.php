<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BindsRepository
 * @package namespace App\Interfaces;
 */
interface BindsRepository extends RepositoryInterface
{
    //
}
