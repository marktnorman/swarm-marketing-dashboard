<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DelimitersRepository
 * @package namespace App\Interfaces;
 */
interface DelimitersRepository extends RepositoryInterface
{
    //
}
