<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CampaignRepository
 * @package namespace App\Interfaces;
 */
interface CampaignRepository extends RepositoryInterface
{
    //
}
