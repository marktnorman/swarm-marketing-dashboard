<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SegmentsStoresRepository
 * @package namespace App\Interfaces;
 */
interface SegmentsStoresRepository extends RepositoryInterface
{
    //
}
