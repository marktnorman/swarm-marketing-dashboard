<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{

    /**
     * @var string
     */
    protected $table = 'jobs';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $fillable = [
        'campaign_id',
        'available_at'
    ];
}
