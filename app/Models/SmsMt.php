<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SmsMt extends Model
{

    /**
     * @var string
     */
    protected $table = 'sms_mt';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $fillable = [
        'msisdn',
        'message',
        'from',
        'campaign_id',
        'bind_id',
        'bind_reference',
        'status'
    ];
}