<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Binds extends Model implements Transformable
{
    use TransformableTrait, SoftDeletes;

    /**
     * @var string
     */
    protected $modelName = 'Binds';

    /**
     * @var array
     */
    protected $fillable = [
        'bind_title',
        'config'
    ];

    /**
     * @return string
     */
    public function getModelName()
    {
        return $this->modelName;
    }

    // public function campaign() {
    //     return $this->hasMany('App\Models\Campaign');
    // }
}
