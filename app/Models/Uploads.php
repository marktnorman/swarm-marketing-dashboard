<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Uploads extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * @var string
     */
    protected $modelName = 'Uploads';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @return string
     */
    public function getModelName()
    {
        return $this->modelName;
    }

}
