<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Lists
 * @package App\Models
 */
class Lists extends Model
{


    /**
     * @var string
     */
    protected $modelName = 'ListCategory';
    /**
     * @var string
     */
    protected $table = 'opt_list';

    /**
     * @var array
     */
    protected $fillable = [
        'list',
        'category'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function listCategory(){
        return $this->belongsTo('App\Models\ListCategory', 'category');
    }

}
