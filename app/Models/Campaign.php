<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Campaign
 * @package App\Models
 */
class Campaign extends Model
{

    use SoftDeletes, TransformableTrait;

    // JPR: These are hardcoded constants for now, will need to change this when the lists table is all up and running
    /**
     *
     */
    const SCHEDULED = 0;
    /**
     *
     */
    const EXECUTED = 1;
    /**
     *
     */
    const CANCELLED = 2;
    /**
     *
     */
    const LOCKED = 3;

    /**
     * @var string
     */
    protected $modelName = 'Campaign';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $fillable = [
        'title',
        'copy',
        'from',
        'send_date',
        'segment_id',
        'bind_id',
        'campaign_type',
        'service_id',
        'campaign_sent',
        'campaign_type',
        'status'
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @return string
     */
    public function getModelName()
    {
        return $this->modelName;
    }

    /**
     * @return $this
     */
    public function getInfo() {
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bind() {
        return $this->belongsTo('App\Models\Binds');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function segment() {
        return $this->belongsTo('App\Models\Segments');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function campaignType() {
        return $this->belongsTo('App\Models\CampaignType', 'campaign_type');
    }

    /**
     * @return null
     */
    public function getBindTitleAttribute()
    {
        if ($this->bind) {
            return $this->bind->bind_title;
        }
        return null;
    }

    /**
     * @return null
     */
    public function getSegmentTitleAttribute()
    {
        if ($this->segment){
            return $this->segment->title;
        }
        return null;
    }

    /**
     * @return null
     */
    public function getCampaignTypeNameAttribute()
    {
        if ($this->campaignType){
            return $this->campaignType->name;
        }
        return null;
    }


    /**
     * @param $value
     */
    public function setFromAttribute($value) {
        if ($value !== '') {
            $this->attributes['from'] = $value;
        }
    }
}
