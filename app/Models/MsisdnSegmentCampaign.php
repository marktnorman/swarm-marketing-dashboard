<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MsisdnSegmentCampaign
 * @package App\Models
 */
class MsisdnSegmentCampaign extends Model
{

    /**
     * @var string
     */
    protected $table = 'msisdn_segment_campaign';
    /**
     * @var string
     */
    protected $modelName = 'MsisdnSegmentCampaign';

    /**
     * @var array
     */
    protected $fillable = [
        'msisdn_id',
        'segment_id',
        'campaign_id'
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];
}
