<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ChunkPart extends Model implements Transformable
{
    use TransformableTrait;

    protected $modelName = 'ChunkPart';

    protected $fillable = [
        'chunk_masters_id',
        'part_num',
        'data'
    ];



    public function getModelName()
    {
        return $this->modelName;
    }

    public function chunkMaster()
    {
        return $this->belongsTo('App\Models\ChunkMaster', 'chunk_masters_id', '');
    }
}
