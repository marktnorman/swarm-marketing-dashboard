<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Delimiters extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * @var string
     */
    protected $modelName = 'Delimiters';

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'type',
        'delimiter_type'
    ];

    /**
     * @return string
     */
    public function getModelName()
    {
        return $this->modelName;
    }

    /**
     * Get the segments associated with the delimiter.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function segments()
    {
        return $this->hasMany('App\Models\Segments', 'delimiter_type');
    }

}
