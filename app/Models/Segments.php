<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Segments extends Model implements Transformable
{

    use TransformableTrait, SoftDeletes;

    // CONSTANTS created for machine readable naming conventions

    //These are for the Stored Attribute
    /**
     *
     */
    const UPLOADABLE = 0;
    /**
     *
     */
    const INPROGRESS = 4;
    /**
     *
     */
    const STORED = 5;
    /**
     *
     */
    const FAILED = 6;

    // These are for the Locked attribute
    /**
     *
     */
    const UNLOCK = 0;
    /**
     *
     */
    const LOCK = 1;

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'file_name',
        'download_url',
        'file_size',
        'type',
        'user_id',
        'delimiter_type',
        'stored',
        'locked',
        'updated_at'
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at', 'updated_at'];


    /**
     * @var string
     */
    protected $modelName = 'Segments';


    /**
     * @return string
     */
    public function getModelName()
    {
        return $this->modelName;
    }

    /**
     * Get the delimiter associated with the segment.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function delimiter()
    {
        return $this->belongsTo('App\Models\Delimiters', 'delimiter_type');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
