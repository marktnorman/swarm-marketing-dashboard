<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MsisdnSegment
 * @package App\Models
 */
class MsisdnSegment extends Model
{

    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'msisdn_id',
        'segment_id',
        'msisdn'
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];
}
