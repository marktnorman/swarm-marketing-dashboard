<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Msisdn extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'msisdn',
        'country_code',
        'mno_id',
        'last_active',
        'channel',
    ];
}
