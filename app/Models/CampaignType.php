<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class CampaignType extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * @var string
     */
    protected $table = 'campaign_type';

    /**
     * @var string
     */
    protected $modelName = 'CampaignType';

    /**
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * @return string
     */
    public function getModelName()
    {
        return $this->modelName;
    }

}
