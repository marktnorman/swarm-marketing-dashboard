<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ChunkMaster extends Model implements Transformable
{
    use TransformableTrait;

    protected $modelName = 'ChunkMaster';

    protected $fillable = [
        'user_id',
        'linked_id',
        'grouping',
        'name',
        'parts',
        'size'
    ];


    protected $dates = ['deleted_at', 'updated_at'];


    public function getModelName()
    {
        return $this->modelName;
    }

    public function chunkParts()
    {
        return $this->hasMany('App\Models\ChunkPart', 'chunk_masters_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

}
