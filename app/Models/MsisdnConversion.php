<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MsisdnConversion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'msisdn_conversion';
}
