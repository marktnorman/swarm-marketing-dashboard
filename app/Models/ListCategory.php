<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class ListCategory
 * @package App\Models
 */
class ListCategory extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * @var string
     */
    protected $modelName = 'ListCategory';

    /**
     * @var array
     */
    protected $fillable = [
        'list'
    ];

    /**
     * @var string
     */
    protected $table = 'list_category';

    /**
     * @return string
     */
    public function getModelName()
    {
        return $this->modelName;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lists(){
        return $this->hasMany('App\Models\Lists', 'category');
    }
}
