<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Lists;

class ListController extends Controller
{
    /**
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Lists::all();
        return view('lists.index', compact('lists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('lists.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Just check if the fields are filled in
        $this->validate($request, [
            'list_option' => 'required'
        ]);

        // Create the list item option
        $inserted = DB::table('opt_list')->insert([
            'list' => $request->list_option
            ]
        );

        if ($inserted) {
            return redirect()->back()->with('message', 'Opt in list item successfully loaded');
        } else {
            return Redirect::back()->withErrors('Something went wrong, please reload your list item');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $list = DB::table('opt_list')->where('id', $id)->first();
        return view('lists.edit', compact('list'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Just check if the fields are filled in
        $this->validate($request, [
            'list_option' => 'required'
        ]);

        // update the opt list item
        DB::table('opt_list')
            ->where('id', $id)
            ->update([
                'list' => $request->list_option
            ]);

        return redirect()->back()->with('message', 'Opt in list item successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
