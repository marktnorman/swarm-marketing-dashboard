<?php

namespace App\Http\Controllers;

use App\Http\Requests\CampaignCreateRequest;
use App\Http\Requests\CampaignUpdateRequest;
use App\Models\Campaign;
use Dingo\Api\Dispatcher;
use DB;

class CampaignController extends Controller
{
    protected $api;


    public function __construct(Dispatcher $api)
    {
        $this->middleware('auth');
        $this->api = $api;
    }


    public function index()
    {
        //JPR - Reverted this to not using the API because of divergence between API and Blade workings
        $campaigns['data'] = Campaign::all();
        return view('campaigns.index', compact('campaigns'));
    }

    public function create()
    {
        $times = config('swarm-data.times');
        $segments = DB::table('segments')->pluck('title', 'id');
        $binds = DB::table('binds')->pluck('bind_title', 'id');
        $campaign_type = DB::table('campaign_type')->pluck('name', 'id');
        return view('campaigns.create', compact('segments', 'binds', 'campaign_type', 'times'));
    }


    public function store(CampaignCreateRequest $request)
    {

        try {
            $this->api->be(auth()->user())->with([
                'title' => $request->title,
                'copy' => $request->copy,
                'bind_id' => $request->bind_id,
                'send_date' => $request->send_date,
                'campaign_time' => $request->campaign_time,
                'segment_id' => $request->segment_id,
                'campaign_type_id' => $request->campaign_type_id,
                'service_id' => $request->service_id,
                'from' => $request->from
            ])->post("api/v1/campaigns");
            return redirect()->back()->with('message', 'Campaign successfully loaded');
        } catch (\Exception $error) {
            return redirect()->back()->withErrors($error->getMessage());
        }

    }

    public function show($id)
    {
        $object = DB::table('campaigns')->where('id', $id)->first();
        $name = $object->title;
        return view('campaigns.show', compact('name'));
    }


    public function edit(Campaign $campaign)
    {

        $times = config('swarm-data.times');
        $segments = DB::table('segments')->pluck('title', 'id');
        $binds = DB::table('binds')->pluck('bind_title', 'id');
        $campaign_type = DB::table('campaign_type')->pluck('name', 'id');
        $campaign_object = DB::table('campaigns')->where('id', $campaign->id)->first();
        $date = $campaign_object->send_date;
        $date = explode(' ', $date);
        $datestamp = $date[0];
        $timestamp = $date[1];

        return view('campaigns.edit', compact('campaign', 'segments', 'binds', 'campaign_type', 'timestamp', 'datestamp', 'times'));
    }


    public function update(CampaignUpdateRequest $request, $id)
    {
        try{
            $this->api->be(auth()->user())->with([
                'title' => $request->title,
                'copy' => $request->copy,
                'bind_id' => $request->bind_id,
                'send_date' => $request->send_date,
                'campaign_time' => $request->campaign_time,
                'segment_id' => $request->segment_id,
                'campaign_type_id' => $request->campaign_type_id,
                'service_id' => $request->service_id,
                'from' => $request->from
            ])->post("api/v1/campaigns/" . $id);
            return redirect()->back()->with('message', 'Campaign successfully updated');
        } catch (\Exception $error) {
            return redirect()->back()->withErrors($error->getMessage());
        }
    }


    public function destroy($id)
    {
        $campaign = Campaign::find($id);
        $campaign->delete();
        return redirect('/campaigns')->with('message', 'Campaign successfully deleted');
    }
}
