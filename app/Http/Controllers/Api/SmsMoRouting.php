<?php

namespace App\Http\Controllers\Api;

use App\Models\Msisdn;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Campaign;
use App\Models\MsisdnSegmentCampaign;
use App\Models\ListCategory;
use Illuminate\Support\Facades\DB;
use App\Contracts\Facades\SwarmLog;

class SmsMoRouting extends Controller
{
    /**
     * Send the relevant information to SDP
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function sms_mo(Request $request)
    {
        $raw_request = strstr($request->getRequestUri(), '?');

        $msisdn = $request->from;
        $msisdn = str_replace("+","", $msisdn);

        $rawMessage = $request->message;
        $message = str_replace(' ', '', $rawMessage);
        $message = strtolower($message);

        $opt_variations = ListCategory::where('list', '=', 'opt_variations')
            ->first()
            ->lists
            ->pluck('list')
            ->toArray();

        if (in_array($message, $opt_variations)) {
            $msisdn = Msisdn::where("msisdn", '=', $msisdn)->first();
            $msisdnSegmentCampaign = MsisdnSegmentCampaign::select('campaign_id')->where('msisdn_id', '=', isset($msisdn->id) ? $msisdn->id : "0")->orderBy('created_at', 'desc')->first();
            if($msisdnSegmentCampaign){
                $campaign = Campaign::find($msisdnSegmentCampaign->campaign_id);

                // Insert the SMS MO request which has found the campaign associated with the request
                DB::table('sms_mo')->insert([
                    'message' => $message,
                    'msisdn' => $msisdn->id,
                    'campaign_id' => $campaign->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);

                $mcc = $request->mcc;
                $mnc = $request->mnc;

                // Found the MSISDN in the system, let's initiate the opt-in request
                $mno = $this->aggregator($mcc, $mnc);

                // Get cURL resource
                $ch = curl_init();

                // Request URI for forcing subscription
                $request_uri = $_ENV["SDP_ENDPOINT"] . "{$_ENV["SDP_VERSION"]}/user/subscribe?club_id={$campaign->service_id}&club_category_id=0&channel=MO&username={$_ENV["SDP_USERNAME"]}&aggregator_id={$mno}&password={$_ENV["SDP_PASSWORD"]}&msisdn={$msisdn->id}&forced=1&skipdoi=1";

                // Log the request
                SwarmLog::info("sms_mo", $request_uri . ' executed on - ' . date("Y-m-d H:i:s") . " with message: " . $rawMessage);

                // Set url
                curl_setopt($ch, CURLOPT_URL, $request_uri);

                // Set method
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

                // Set options
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                // Send the request
                curl_exec($ch);

                // Close request to clear up some resources
                curl_close($ch);
            }
            else {
                // Nothing found in the system so just pass on the request to the SDP

                // Get cURL resource
                $ch = curl_init();

                // Set url
                curl_setopt($ch, CURLOPT_URL, 'https://hyvesdp.com/callback/panacea/sms' . $raw_request);

                // Log the request
                SwarmLog::info("sms_mo", 'https://hyvesdp.com/callback/panacea/sms' . $raw_request . ' sent along to SDP on - ' . date("Y-m-d H:i:s"));

                // Set method
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

                // Set options
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                // Send the request
                curl_exec($ch);

                //Log the request
                SwarmLog::info("sms_mo",'https://hyvesdp.com/callback/panacea/sms' . $raw_request . ' on this day - ' . date('Y-m-d H:i:s'));

                // Close request to clear up some resources
                curl_close($ch);
            }
        }
        else {
            // Message didn't match any on our given variable spelling, send it along

            // Get cURL resource
            $ch = curl_init();

            // Set url
            curl_setopt($ch, CURLOPT_URL, 'https://hyvesdp.com/callback/panacea/sms' . $raw_request);

            // Log the request
            SwarmLog::info("sms_mo",'https://hyvesdp.com/callback/panacea/sms' . $raw_request . ' sent along to SDP on - ' . date("Y-m-d H:i:s"));

            // Set method
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

            // Set options
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            // Send the request
            curl_exec($ch);

            //Log the request
            SwarmLog::info("sms_mo", 'https://hyvesdp.com/callback/panacea/sms' . $raw_request . ' on this day - ' . date('Y-m-d H:i:s'));

            // Close request to clear up some resources
            curl_close($ch);
        }
    }

    /**
     * Retrieve the aggregator ID mapped to SDP
     *
     * @param  int $mcc, int $mnc
     */
    public function aggregator($mcc, $mnc)
    {
        // Use MCC and MNC to
        if (isset($mcc) && isset($mnc) && $mcc == '655')
        {
            switch ($mnc)
            {
                case '07':
                    $aggregatorId = 1;
                    break;
                case '10':
                case '12':
                    $aggregatorId = 4;
                    break;
                case '01':
                    $aggregatorId = 3;
                    break;
                default:
                    $aggregatorId = null;
                    SwarmLog::warning("sms_mo", "WARNING: Received SMS with unknown MNC ($mnc)");
            }

            // Return the aggregator ID
            return $aggregatorId;

        }
    }
}
