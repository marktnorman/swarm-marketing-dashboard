<?php

namespace App\Http\Controllers\Api;

use App\Models\SmsMt;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\Facades\SwarmLog;

class TotalsendDeliveryReport extends Controller
{
    /**
     * Store the delivery report.
     *
     * @param  Request  $request
     */
    public function store(Request $request)
    {
        $reference = $request->input('mt_ref');
        $status = $request->input('status');
        $status = $this->totalsend_status_mapping($status);

        SwarmLog::info('reports', 'Incoming delivery report: ' . $reference);

        if (isset($reference) && isset($status)) {
            SmsMt::where('bind_reference', $reference)
                ->update(['status' => $status]);
        }
    }

    /**
     * Map status from Totalsend to SDP configuration
     *
     * @param  $status
     *
     * 1 – Delivered
     * 2 – Undelivered
     * 4 – Queued%at%network
     * 8 – Sent%to%network
     * 16 – Failed%at%network
     *
     * SENDING = 1;
     * SUBMITTED = 2;
     * DELIVERED = 4;
     * FAILED = 8;
     * EXPIRED = 16;
     * REJECTED = 32;
     *
     * 99 means we've received a status from Totalsend which is unknown
     */
    public function totalsend_status_mapping($status)
    {
        switch ($status) {
            case 1:
                $status = 4;
                break;
            case 2:
                $status = 16;
                break;
            case 4:
                $status = 1;
                break;
            case 8:
                $status = 2;
                break;
            case 16:
                $status = 8;
                break;
            case -32:
                $status = 32;
                break;
            default:
                $status = 99;
        }

        return $status;

    }
}
