<?php

namespace App\Http\Controllers;

use App\Http\Requests\SegmentsCreateRequest;
use Dingo\Api\Dispatcher;
use Illuminate\Http\Request;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Models\Segments;

class SegmentController extends Controller
{
    /**
     * @var Dispatcher
     */
    protected $api;


    /**
     * SegmentController constructor.
     * @param Dispatcher $api
     */
    public function __construct(Dispatcher $api)
    {
        $this->middleware('auth');
        $this->api = $api;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $segments = $this->api->be(auth()->user())->get("api/v1/segments");
        return view('segments.index', compact('segments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $delimiters = DB::table('delimiters')->pluck('type', 'id');
        return view('segments.create', compact('delimiters'));
    }


    /**
     * @param SegmentsCreateRequest $request
     * @return mixed
     */
    public function store(SegmentsCreateRequest $request)
    {
        try {
            if (File::exists($request->file('file_name'))) {

                // Set request info
                $uploadedFile = $request->file('file_name');
                $file_size = filesize($uploadedFile);
                $filename = time() . '-csv-import.csv';
                $url = 'csv-files/current/' . $filename;
                $full_url = Storage::disk('s3')->url($url);

                // Store the file on S3
                Storage::disk('s3')->put($url, file_get_contents($uploadedFile), 'public');
                $this->api->be(auth()->user())->with([
                    'title' => $request->title,
                    'file_name' => $filename,
                    'type' => 1,
                    'delimiter_type' => $request->delimiter_type,
                    'user' => auth()->user(),
                    'download_url' => $full_url,
                    'file_size' => $file_size
                    ])->post("api/v1/segments");
                return redirect('segments')->with('message', 'Segment successfully loaded, please be patient while we process the CSV, status on segment listing view will indicate process.');
            }
        } catch
        (\Exception $error) {
            return redirect()->back()->withErrors('Something went wrong please try again', $error->getMessage());
        }
    }


    public function edit(Segments $segment)
    {
        $delimiters = DB::table('delimiters')->pluck('type', 'id');

        return view('segments.edit', compact('segment', 'delimiters'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->api->be(auth()->user())->with([
                'id' => $id,
                'title' => $request->title,
                'type' => 1,
                'user' => auth()->user(),
                'delimiter_type' => $request->delimiter_type
            ])->post("api/v1/segments/" . $id);
            return redirect('segments')->with('message', 'Segment successfully updated');
        } catch (\Exception $error) {
            return redirect()->back()->withErrors('Something went wrong, please reload your Segment');
        }
    }
}
