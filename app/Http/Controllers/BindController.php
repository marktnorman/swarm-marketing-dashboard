<?php

namespace App\Http\Controllers;

use App\Http\Requests\BindsCreateRequest;
use App\Http\Requests\BindsUpdateRequest;
use Dingo\Api\Dispatcher;

class BindController extends Controller
{
    /**
     * @var Dispatcher
     */
    protected $api;

    /**
     * BindController constructor.
     * @param Dispatcher $api
     */
    public function __construct(Dispatcher $api)
    {
        $this->middleware('auth');
        $this->api = $api;
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $binds = $this->api->be(auth()->user())->get('api/v1/binds');
        return view('binds.index', compact('binds'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('binds.create');
    }


    /**
     * @param BindsCreateRequest $request
     * @return mixed
     */
    public function store(BindsCreateRequest $request)
    {
        try {
            $this->api->be(auth()->user())->with([
                'bind_title' => $request->bind_title,
                'bind_username' => $request->bind_username,
                'bind_password' => $request->bind_password,
                'api_endpoint_url' => $request->api_endpoint_url
            ])->post("api/v1/binds/");
            return redirect('binds')->with('message', 'Bind successfully uploaded');
        } catch (\Exception $error) {
            return redirect()->back()->withErrors('Something went wrong, please reload your bind');
        }
    }


    public function show($id)
    {
        $name = $this->api->be(auth()->user())->get('api/v1/binds/1')->title;
        return view('campaigns.show', compact('name'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $bind = $this->api->be(auth()->user())->get('api/v1/binds/' . $id);
        return view('binds.edit', compact('bind'));
    }

    /**
     * @param BindsUpdateRequest $request
     * @param $id
     * @return mixed
     */
    public function update(BindsUpdateRequest $request, $id)
    {
        try {
            $this->api->be(auth()->user())->with([
                'bind_title' => $request->bind_title,
                'bind_username' => $request->bind_username,
                'bind_password' => $request->bind_password,
                'api_endpoint_url' => $request->api_endpoint_url
            ])->post("api/v1/binds/" . $id);
            return redirect('binds')->with('message', 'Bind successfully updated');
        } catch (\Exception $error) {
            return redirect()->back()->withErrors('Something went wrong, please reload your bind');
        }
    }
}
