<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BindsUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bind_title' => 'required',
            'bind_username' => 'required',
            'bind_password' => 'required',
            'api_endpoint_url' => 'required|url'
        ];
    }
}
