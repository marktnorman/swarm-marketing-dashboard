<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CampaignUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'copy' => 'required',
            'send_date' => 'required',
            'segment_id' => 'required',
            'bind_id' => 'required',
            'campaign_type_id' => 'required',
            'service_id' => 'required'
        ];
    }
}
