<?php

namespace App\Http\Api\V1\Controllers;

use App\Http\Api\ApiController;

use App\Models\CampaignType;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Presenters\CampaignTypePresenter;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\CampaignTypeCreateRequest;
use App\Http\Requests\CampaignTypeUpdateRequest;
use App\Interfaces\CampaignTypeRepository;
use App\Validators\CampaignTypeValidator;


class CampaignTypesController extends ApiController
{

    /**
     * @var CampaignTypeRepository
     */
    protected $repository;

    /**
     * @var CampaignTypeValidator
     */
    protected $validator;

    public function __construct(CampaignTypeRepository $repository, CampaignTypeValidator $validator, CampaignTypePresenter $presenter)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->repository->setPresenter($presenter);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $campaignTypes = $this->repository->all();
            return $this->respondWithSuccess($campaignTypes);

        } catch (\Exception $error) {
            return $this->respondInternalError($error->getMessage());
        }
    }
}

 // Documentation

 /**
 * @SWG\Get(
 *      path="/campaignTypes",
 *      operationId="get_all_campaignTypes",
 *      tags={"CampaignTypes"},
 *      summary="Get list of all campaignTypes",
 *      description="Returns list of campaignTypes",
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation",
 *       ),
 *  )
 *
 */