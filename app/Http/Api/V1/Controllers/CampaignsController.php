<?php

namespace App\Http\Api\V1\Controllers;

use App\Http\Api\ApiController;

use App\Models\Campaign;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Presenters\CampaignPresenter;
use Dingo\Api\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use App\Http\Requests\CampaignCreateRequest;
use App\Http\Requests\CampaignUpdateRequest;
use App\Interfaces\CampaignRepository;
use App\Validators\CampaignValidator;


class CampaignsController extends ApiController
{

    /**
     * @var CampaignRepository
     */
    protected $repository;

    /**
     * @var CampaignValidator
     */
    protected $validator;

    /**
     * CampaignsController constructor.
     * @param CampaignRepository $repository
     * @param CampaignValidator $validator
     * @param CampaignPresenter $presenter
     */
    public function __construct(CampaignRepository $repository, CampaignValidator $validator, CampaignPresenter $presenter)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->repository->setPresenter($presenter);
    }

    /**
     * Display a listing of the resource scoped by status scheduled
     *
     * @param Request $request
     * @param int $limit
     * @param array $columns
     * @return mixed
     */
    public function indexByScheduled(Request $request, $limit = 10, $columns = ['*'])
    {
        try {
            $campaigns =
                $this->repository->scopeQuery(function($query){
                    return $query->where('status', Campaign::SCHEDULED);
                })->orderBy('updated_at', 'desc');
            if(isset($request->paginate) && $request->paginate == true){
                $campaigns = $campaigns->paginate(isset($request->limit)? $request->limit : $limit, $columns);
            } else {
                $campaigns = $campaigns->all();
            }

            return $this->respondWithSuccess($campaigns);

        } catch(\Exception $error) {
            return $this->respondInternalError($error->getMessage());
        }
    }

    /**
     * Display a listing of the resource scoped by status executed
     *
     * @param Request $request
     * @param int $limit
     * @param array $columns
     * @return mixed
     */
    public function indexByExecuted(Request $request, $limit = 10, $columns = ['*'])
    {
        try {
            $campaigns =
                $this->repository->scopeQuery(function($query){
                    return $query->where('status', Campaign::EXECUTED);
                })->orderBy('updated_at', 'desc');
            if(isset($request->paginate) && $request->paginate == true){
                $campaigns = $campaigns->paginate(isset($request->limit)? $request->limit : $limit, $columns);
            } else {
                $campaigns = $campaigns->all();
            }
            return $this->respondWithSuccess($campaigns);

        } catch(\Exception $error) {
            return $this->respondInternalError($error->getMessage());
        }
    }

    /**
     * Display a listing of the resource scoped by status cancelled
     *
     * @param Request $request
     * @param int $limit
     * @param array $columns
     * @return mixed
     */
    public function indexByCancelled(Request $request, $limit = 10, $columns = ['*'])
    {
        try {
            $campaigns =
                $this->repository->scopeQuery(function($query){
                    return $query->where('status', Campaign::CANCELLED);
                })->orderBy('updated_at', 'desc');
            if(isset($request->paginate) && $request->paginate == true){
                $campaigns = $campaigns->paginate(isset($request->limit)? $request->limit : $limit, $columns);
            } else {
                $campaigns = $campaigns->all();
            }
            return $this->respondWithSuccess($campaigns);

        } catch(\Exception $error) {
            return $this->respondInternalError($error->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $campaign = $this->repository->find($id);
            return $this->respondWithSuccess($campaign);
        } catch(ModelNotFoundException $error){
            return $this->respondNotFound($error->getMessage());
        } catch (\Exception $error){
            return $this->respondInternalError($error->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CampaignCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CampaignCreateRequest $request)
    {
        try {
            if(!$this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE)) {
                return $this->respondUnprocessableEntity($this->errorBag());
            }
            $campaign = $this->repository->create($request->all());
            return $this->respondCreated($campaign);

        } catch(\Exception $error) {
            return $this->respondInternalError($error->getMessage());
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CampaignUpdateRequest $request
     * @param  string            $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(CampaignUpdateRequest $request, $id)
    {
        try {
            if(!$this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE)) {
                return $this->respondUnprocessableEntity($this->errorBag());
            }
            $campaign = $this->repository->update($request->all(), $id);
            return $this->respondCreated($campaign);

        } catch(\Exception $error) {
            return $this->respondInternalError($error->getMessage());
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        try {
            $this->repository->delete($id);
            return $this->respondNoContent();
        } catch(\Exception $error) {
            return $this->respondInternalError($error->getMessage());
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function restore($id)
    {
        try {
            $campaign = Campaign::withTrashed()->find($id);
            $campaign->restore();
            return $this->respondWithSuccess($campaign);
        } catch(\Exception $error) {
            return $this->respondInternalError($error->getMessage());
            }
    }


 // Documentation

 /**
 * @SWG\Get(
 *      path="/campaigns/scheduled",
 *      operationId="get_all_campaigns_by_status_scheduled",
 *      tags={"Campaigns"},
 *      summary="Get list of all campaigns by status scheduled",
 *      description="Returns list of campaigns by status scheduled",
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation",
 *       ),
 *  )
 *
 */

/**
 * @SWG\Get(
 *      path="/campaigns/executed",
 *      operationId="get_all_campaigns_by_status_executed",
 *      tags={"Campaigns"},
 *      summary="Get list of all campaigns by status executed",
 *      description="Returns list of campaigns by status executed",
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation",
 *       ),
 *  )
 *
 */

/**
 * @SWG\Get(
 *      path="/campaigns/cancelled",
 *      operationId="get_all_campaigns_by_status_cancelled",
 *      tags={"Campaigns"},
 *      summary="Get list of all campaigns by status cancelled",
 *      description="Returns list of campaigns by status cancelled",
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation",
 *       ),
 *  )
 *
 */

/**
 * @SWG\Get(
 *      path="/campaigns/1",
 *      operationId="get_single_campaign",
 *      tags={"Campaigns"},
 *      summary="Get single campaign",
 *      description="Returns a single campaign",
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation",
 *       )
 * )
 *
 */

/**
 * @SWG\Post(
 *     path="/campaigns",
 *     tags={"Campaigns"},
 *     operationId="create_campaign",
 *     summary="Create a campaign",
 *     description="Creates a campaign given the form request",
 *     @SWG\Parameter(
 *         name="title",
 *         in="formData",
 *         type="string",
 *         description="",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="copy",
 *         in="formData",
 *         type="string",
 *         description="",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="send_date",
 *         in="formData",
 *         type="string",
 *         description="",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="segment_id",
 *         in="formData",
 *         type="integer",
 *         description="",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="bind_id",
 *         in="formData",
 *         type="integer",
 *         description="",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="campaign_type_id",
 *         in="formData",
 *         type="integer",
 *         description="",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="service_id",
 *         in="formData",
 *         type="integer",
 *         description="",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="from",
 *         in="formData",
 *         type="integer",
 *         description="",
 *     ),
 *     @SWG\Response(
 *          response=201,
 *          description="successful operation",
 *       )
 * )
 */

/**
 * @SWG\Post(
 *     path="/campaigns/1",
 *     tags={"Campaigns"},
 *     operationId="update_campaign",
 *     summary="Update a campaign",
 *     description="Update a campaign given the form request",
 *     @SWG\Parameter(
 *         name="title",
 *         in="formData",
 *         type="string",
 *         description="",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="copy",
 *         in="formData",
 *         type="string",
 *         description="",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="send_date",
 *         in="formData",
 *         type="string",
 *         description="",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="segment_id",
 *         in="formData",
 *         type="integer",
 *         description="",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="bind_id",
 *         in="formData",
 *         type="integer",
 *         description="",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="campaign_type_id",
 *         in="formData",
 *         type="integer",
 *         description="",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="service_id",
 *         in="formData",
 *         type="integer",
 *         description="",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="from",
 *         in="formData",
 *         type="integer",
 *         description="",
 *     ),
 *     @SWG\Response(
 *          response=201,
 *          description="successful operation",
 *       )
 * )
 */
}
