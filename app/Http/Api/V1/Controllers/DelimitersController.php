<?php

namespace App\Http\Api\V1\Controllers;

use App\Http\Api\ApiController;

use App\Models\Delimiters;
use Dingo\Api\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Presenters\DelimitersPresenter;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\DelimitersCreateRequest;
use App\Http\Requests\DelimitersUpdateRequest;
use App\Interfaces\DelimitersRepository;
use App\Validators\DelimitersValidator;


class DelimitersController extends ApiController
{

    /**
     * @var DelimitersRepository
     */
    protected $repository;

    /**
     * @var DelimitersValidator
     */
    protected $validator;

    public function __construct(DelimitersRepository $repository, DelimitersValidator $validator, DelimitersPresenter $presenter)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->repository->setPresenter($presenter);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            ( isset($request->skipPresenter) ?
                $delimiters = $this->repository->skipPresenter()->all()
                :
                $delimiters = $this->repository->all()
            );

            return $this->respondWithSuccess($delimiters);

        } catch (\Exception $error) {
            return $this->respondInternalError($error->getMessage());
        }
    }
}


 // Documentation

 /**
 * @SWG\Get(
 *      path="/delimiters",
 *      operationId="get_all_delimiters",
 *      tags={"Delimiters"},
 *      summary="Get list of all delimiters",
 *      description="Returns list of delimiters",
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation",
 *       ),
 *  )
 *
 */