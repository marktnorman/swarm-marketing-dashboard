<?php

namespace App\Http\Api\V1\Controllers;

use App\Http\Api\ApiController;

use App\Jobs\SegmentsStoreMaster;
use App\Models\Segments;
use Dingo\Api\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Presenters\SegmentsPresenter;
use Prettus\Validator\Contracts\ValidatorInterface;
use App\Http\Requests\SegmentsCreateRequest;
use App\Http\Requests\SegmentsUpdateRequest;
use App\Interfaces\SegmentsRepository;
use App\Validators\SegmentsValidator;
use Tymon\JWTAuth\Facades\JWTAuth;


/**
 * Class SegmentsController
 * @package App\Http\Api\V2\Controllers
 */
class SegmentsController extends ApiController
{

    /**
     * @var SegmentsRepository
     */
    protected $repository;

    /**
     * @var SegmentsValidator
     */
    protected $validator;

    public function __construct(SegmentsRepository $repository, SegmentsValidator $validator, SegmentsPresenter $presenter)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->repository->setPresenter($presenter);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param int $limit
     * @param array $columns
     * @return mixed
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $limit = 10, $columns = ['*'])
    {

        try {
            if(isset($request->paginate) && $request->paginate == true){

                $segments = $this->repository->orderBy('updated_at', 'desc')->paginate(isset($request->limit)? $request->limit : $limit, $columns);
                return $this->respondWithSuccess($segments);
            }
            $segments = $this->repository->orderBy('updated_at', 'desc')->all();

            return $this->respondWithSuccess($segments);

        } catch(\Exception $error) {
            return $this->respondInternalError($error->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $segment = $this->repository->find($id);
            return $this->respondWithSuccess($segment);
        } catch(ModelNotFoundException $error){
            return $this->respondNotFound($error->getMessage());
        } catch (\Exception $error){
            return $this->respondInternalError($error->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  SegmentsCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SegmentsCreateRequest $request)
    {

        try {
            $segmentData = $this->buildSegment($request->all());
            if(!$this->validator->with($segmentData)->passesOrFail(ValidatorInterface::RULE_CREATE)) {
                return $this->respondUnprocessableEntity($this->errorBag());
            }

            $segment = $this->repository->create($segmentData);
            SegmentsStoreMaster::dispatch($segment['data']['id'], 5000)->onQueue('StoreSegments');;
            return $this->respondCreated($segment);

        } catch(\Exception $error) {
            return $this->respondInternalError($error->getMessage());
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  SegmentsUpdateRequest $request
     * @param  string            $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(SegmentsUpdateRequest $request, $id)
    {
        try {
            $segmentData = $this->updateSegment($request->all());
            if(!$this->validator->with($segmentData)->passesOrFail(ValidatorInterface::RULE_CREATE)) {
                return $this->respondUnprocessableEntity($this->errorBag());
            }
            $segment = $this->repository->update($segmentData, $id);
            return $this->respondCreated($segment);

        } catch(\Exception $error) {
            return $this->respondInternalError($error->getMessage());
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        try {
            $this->repository->delete($id);
            return $this->respondNoContent();
        } catch(\Exception $error) {
            return $this->respondInternalError($error->getMessage());
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function restore($id)
    {
        try {
            $segment = Segments::withTrashed()->find($id);
            $segment->restore();
            return $this->respondWithSuccess($segment);
        } catch(\Exception $error) {
            return $this->respondInternalError($error->getMessage());
            }
    }

    /**
     * @param $data
     * @return array
     */
    private function buildSegment($data)
    {
        // ToDo - JPR: Remove this locked state from the DB entirely. It's currently in to allow for a change over
        $segmentData = [
            'title' => $data['title'],
            'file_name' =>  $data['file_name'],
            'type' =>  null,
            'user_id' =>  isset($data['user']->id) ? $data['user']->id : $this->getUser()->id,
            'download_url' => $data['download_url'],
            'file_size' => $data['file_size'],
            'locked' => '1',
            'delimiter_type' =>isset($data['delimiter_type']) ? $data['delimiter_type'] : '1',
        ];
        return $segmentData;
    }

    /**
     * @param $data
     * @return array
     */
    private function updateSegment($data)
    {
        $segmentData = [
            'title' => $data['title'],
            'type' =>  null,
            'user_id' =>  isset($data['user']->id) ? $data['user']->id : $this->getUser()->id,
        ];
        return $segmentData;
    }


 // Documentation

 /**
 * @SWG\Get(
 *      path="/segments",
 *      operationId="get_all_segments",
 *      tags={"Segments"},
 *      summary="Get list of all segments",
 *      description="Returns list of segments",
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation",
 *       ),
 *  )
 *
 */

/**
 * @SWG\Get(
 *      path="/segments/1",
 *      operationId="get_single_segment",
 *      tags={"Segments"},
 *      summary="Get single segment",
 *      description="Returns a single segment",
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation",
 *       )
 * )
 *
 */

/**
 * @SWG\Post(
 *     path="/segments",
 *     tags={"Segments"},
 *     operationId="create_segment",
 *     summary="Create a segment",
 *     description="Creates a segment given the form request",
 *     @SWG\Parameter(
 *         name="title",
 *         in="formData",
 *         type="string",
 *         description="",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="delimiter_type",
 *         in="formData",
 *         type="integer",
 *         description="",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="file_name",
 *         in="formData",
 *         type="string",
 *         description="",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="download_url",
 *         in="formData",
 *         type="string",
 *         description="",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="file_size",
 *         in="formData",
 *         type="integer",
 *         description="",
 *         required=true,
 *     ),
 *     @SWG\Response(
 *          response=201,
 *          description="successful operation",
 *       )
 * )
 */

/**
 * @SWG\Post(
 *     path="/segments/1",
 *     tags={"Segments"},
 *     operationId="update_segment",
 *     summary="Update a segment",
 *     description="Update a segment given the form request",
 *     @SWG\Parameter(
 *         name="name",
 *         in="formData",
 *         type="string",
 *         description="",
 *         required=true,
 *     ),
 *     @SWG\Response(
 *          response=201,
 *          description="successful operation",
 *       )
 * )
 */

/**
 * @SWG\Post(
 *     path="/segments/1/delete",
 *     tags={"Segments"},
 *     operationId="delete_segment",
 *     summary="Delete a segment",
 *     description="Delete a segment",
 *     @SWG\Response(
 *          response=204,
 *          description="successful operation",
 *       )
 * )
 */

/**
 * @SWG\Post(
 *     path="/segments/1/recover",
 *     tags={"Segments"},
 *     operationId="recover_segment",
 *     summary="Recover a segment",
 *     description="Recover a segment",
 *     @SWG\Response(
 *          response=200,
 *          description="successful operation",
 *       )
 * )
 */
}
