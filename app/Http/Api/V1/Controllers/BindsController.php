<?php

namespace App\Http\Api\V1\Controllers;

use App\Http\Api\ApiController;

use App\Models\Binds;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Presenters\BindsPresenter;
use Dingo\Api\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\BindsCreateRequest;
use App\Http\Requests\BindsUpdateRequest;
use App\Interfaces\BindsRepository;
use App\Validators\BindsValidator;
use Illuminate\Support\Facades\Crypt;


class BindsController extends ApiController
{

    /**
     * @var BindsRepository
     */
    protected $repository;

    /**
     * @var BindsValidator
     */
    protected $validator;

    public function __construct(BindsRepository $repository, BindsValidator $validator, BindsPresenter $presenter)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->repository->setPresenter($presenter);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param int $limit
     * @param array $columns
     * @return mixed
     */
    public function index(Request $request, $limit = 10, $columns = ['*'])
    {
        try {
            if(isset($request->paginate) && $request->paginate == true){
                $binds = $this->repository->paginate(isset($request->limit)? $request->limit : $limit, $columns);
                return $this->respondWithSuccess($binds);
            }
            $binds = $this->repository->orderBy('updated_at', 'desc')->all();

            return $this->respondWithSuccess($binds);

        } catch(\Exception $error) {
            return $this->respondInternalError($error->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $bind = $this->repository->find($id);
            return $this->respondWithSuccess($bind);
        } catch(ModelNotFoundException $error){
            return $this->respondNotFound($error->getMessage());
        } catch (\Exception $error){
            return $this->respondInternalError($error->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  BindsCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(BindsCreateRequest $request)
    {
        try {
            $bindData = $this->buildBind($request->all());
            if(!$this->validator->with($bindData)->passesOrFail(ValidatorInterface::RULE_CREATE)) {
                return $this->respondUnprocessableEntity($this->errorBag());
            }
            $bind = $this->repository->create($bindData);
            return $this->respondCreated($bind);

        } catch(\Exception $error) {
            return $this->respondInternalError($error->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  BindsUpdateRequest $request
     * @param  string            $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(BindsUpdateRequest $request, $id)
    {
        try {
            $bindData = $this->buildBind($request->all());
            if(!$this->validator->with($bindData)->passesOrFail(ValidatorInterface::RULE_CREATE)) {
                return $this->respondUnprocessableEntity($this->errorBag());
            }
            $bind = $this->repository->update($bindData, $id);
            return $this->respondCreated($bind);

        } catch(\Exception $error) {
            return $this->respondInternalError($error->getMessage());
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        try {
            $this->repository->delete($id);
            return $this->respondNoContent();
        } catch(\Exception $error) {
            return $this->respondInternalError($error->getMessage());
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function restore($id)
    {
        try {
            $bind = Binds::withTrashed()->find($id);
            $bind->restore();
            return $this->respondWithSuccess($bind);
        } catch(\Exception $error) {
            return $this->respondInternalError($error->getMessage());
            }
    }

    /**
     * @param $data
     * @return array
     */
    private function buildBind($data)
    {
        $password = Crypt::encryptString($data['bind_password']);
        
        $configData = [
            'bind_username' => $data['bind_username'],
            'bind_password' => $password,
            'api_endpoint_url' => $data['api_endpoint_url']
        ];

        $bindData = [
            'bind_title' => $data['bind_title'],
            'config' => json_encode($configData)
        ];
        return $bindData;
    }


 // Documentation

 /**
 * @SWG\Get(
 *      path="/binds",
 *      operationId="get_all_binds",
 *      tags={"Binds"},
 *      summary="Get list of all binds",
 *      description="Returns list of binds",
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation",
 *       ),
 *  )
 *
 */

/**
 * @SWG\Get(
 *      path="/binds/1",
 *      operationId="get_single_bind",
 *      tags={"Binds"},
 *      summary="Get single bind",
 *      description="Returns a single bind",
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation",
 *       ),
 * )
 *
 */

/**
 * @SWG\Post(
 *     path="/binds",
 *     tags={"Binds"},
 *     operationId="create_bind",
 *     summary="Create a bind",
 *     description="Creates a bind given the form request",
 *     @SWG\Parameter(
 *         name="name",
 *         in="formData",
 *         type="string",
 *         description="",
 *         required=true,
 *     ),
 *     @SWG\Response(
 *          response=201,
 *          description="successful operation",
 *       )
 * )
 */

/**
 * @SWG\Post(
 *     path="/binds/1",
 *     tags={"Binds"},
 *     operationId="update_bind",
 *     summary="Update a bind",
 *     description="Update a bind given the form request",
 *     @SWG\Parameter(
 *         name="name",
 *         in="formData",
 *         type="string",
 *         description="",
 *         required=true,
 *     ),
 *     @SWG\Response(
 *          response=201,
 *          description="successful operation",
 *       )
 * )
 */

/**
 * @SWG\Post(
 *     path="/binds/1/delete",
 *     tags={"Binds"},
 *     operationId="delete_bind",
 *     summary="Delete a bind",
 *     description="Delete a bind",
 *     @SWG\Response(
 *          response=204,
 *          description="successful operation",
 *       )
 * )
 */

/**
 * @SWG\Post(
 *     path="/binds/1/recover",
 *     tags={"Binds"},
 *     operationId="recover_bind",
 *     summary="Recover a bind",
 *     description="Recover a bind",
 *     @SWG\Response(
 *          response=200,
 *          description="successful operation",
 *       )
 * )
 */
}
