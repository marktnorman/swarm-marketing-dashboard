<?php

namespace App\Http\Api\V1\Controllers;

use App\Http\Api\ApiController;
use Illuminate\Support\Facades\File;
use App\Presenters\UploadsPresenter;
use App\Http\Requests\UploadsStoreCSVRequest;
use App\Interfaces\UploadsRepository;
use App\Validators\UploadsValidator;
use Illuminate\Support\Facades\Storage;


class UploadsController extends ApiController
{

    /**
     * @var UploadsRepository
     */
    protected $repository;

    /**
     * @var UploadsValidator
     */
    protected $validator;

    public function __construct(UploadsRepository $repository, UploadsValidator $validator, UploadsPresenter $presenter)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->repository->setPresenter($presenter);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UploadsStoreCSVRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function storeCSV(UploadsStoreCSVRequest $request)
    {
        try {
            if(!$request->file('csv_file')) {
                return $this->respondHttpConflict('Request needs to have a file');
            }
            $uploadedFile = $request->file('csv_file');
            $filename = time() . '-csv-import.' . $uploadedFile->getClientOriginalExtension();
            $url = 'csv-files/current/' . $filename;

            // Save the files on S3
            Storage::disk('s3')->put($url, file_get_contents($uploadedFile), 'public');

            return $this->respondWithSuccess([
                'data' => [
                    'file_name' => $uploadedFile->getClientOriginalName(),
                    'file_size' => $uploadedFile->getClientSize(),
                    'url' => Storage::disk('s3')->url($url),
                    'updated_file_name' => $filename
                ]
            ]);

        } catch(\Exception $error) {
            return $this->respondInternalError($error->getMessage());
        }
    }


 // Documentation

/**
 * @SWG\Post(
 *     path="/upload/csv",
 *     tags={"Uploads"},
 *     operationId="create_uploaded_csv",
 *     summary="Create a new csv upload",
 *     description="Creates a new csv upload given the file",
 *     @SWG\Parameter(
 *         name="csv_file",
 *         in="formData",
 *         type="string",
 *         description="",
 *         required=true,
 *     ),
 *     @SWG\Response(
 *          response=200,
 *          description="successful operation",
 *       )
 * )
 */
}
