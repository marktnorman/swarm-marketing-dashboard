<?php


namespace App\Http\Api\V1\Controllers;


use App\Http\Api\ApiController;
use App\Models\User;

/**
 * Class NotificationsController
 * @package App\Http\Api\V1\Controllers
 */
class NotificationsController extends ApiController
{

    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param $user_id
     * @param $notification_id
     * @return mixed
     */
    public function markAsRead($user_id, $notification_id)
    {
        try {
            $currentUser = $this->user->find($user_id);
            $notification = $currentUser->notifications()->find($notification_id);
            if(!$notification) {
                return $this->respondNotFound("There is no notification with the ID: $notification_id");
            }
            $notification->markAsRead();
            return $this->respondNoContent();
        } catch (\Exception $error) {
            return $this->respondInternalError($error->getMessage());
        }

    }

}


// Documentation

/**
 * @SWG\Post(
 *      path="/notifications/1/1",
 *      operationId="mark_notification_as_read",
 *      tags={"Notifications"},
 *      summary="Mark a notification as read",
 *      description="Mark a notification as read, takes user_id and notification_id",
 *      @SWG\Response(
 *          response=204,
 *          description="successful operation",
 *       ),
 *  )
 *
 */