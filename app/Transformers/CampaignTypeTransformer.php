<?php

namespace App\Transformers;

use Illuminate\Support\Facades\Lang;
use League\Fractal\TransformerAbstract;
use App\Models\CampaignType;

/**
 * Class CampaignTypeTransformer
 * @package namespace App\Transformers;
 */
class CampaignTypeTransformer extends TransformerAbstract
{

    /**
     * Transform the \CampaignType entity
     * @param CampaignType $model
     *
     * @return array
     */
    public function transform(CampaignType $model)
    {
        $data = array_only($model->toArray(), $model->getFillable());
        return [
            'id'         => (int) $model->id,
            'type' => $model->getModelName(),
            'attributes' => $data
        ];
    }
}
