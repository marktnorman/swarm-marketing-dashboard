<?php

namespace App\Transformers;

use Illuminate\Support\Facades\Lang;
use League\Fractal\TransformerAbstract;
use App\Models\Uploads;

/**
 * Class UploadsTransformer
 * @package namespace App\Transformers;
 */
class UploadsTransformer extends TransformerAbstract
{

    /**
     * Transform the \Uploads entity
     * @param Uploads $model
     *
     * @return array
     */
    public function transform(Uploads $model)
    {
        $data = array_only($model->toArray(), $model->getFillable());
        return [
            'id'         => (int) $model->id,
            'type' => $model->getModelName(),
            'attributes' => $data
        ];
    }
}
