<?php

namespace App\Transformers;

use Illuminate\Support\Facades\Lang;
use League\Fractal\TransformerAbstract;
use App\Models\Campaign;

/**
 * Class CampaignTransformer
 * @package namespace App\Transformers;
 */
class CampaignTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'campaignType'
    ];
    /**
     * Transform the \Campaign entity
     * @param Campaign $model
     *
     * @return array
     */
    public function transform(Campaign $model)
    {
        $data = array_only($model->toArray(), $model->getFillable());
        $data['bind_title'] = $model->getBindTitleAttribute();
        $data['segment_title'] = $model->getSegmentTitleAttribute();
        $data['campaign_type_name'] = $model->getCampaignTypeNameAttribute();

        return [
            'id' => (int) $model->id,
            'type' => $model->getModelName(),
            'attributes' => $data
        ];
    }
}
