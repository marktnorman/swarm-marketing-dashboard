<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Binds;
use Illuminate\Support\Facades\Crypt;

/**
 * Class BindsTransformer
 * @package namespace App\Transformers;
 */
class BindsTransformer extends TransformerAbstract
{

    /**
     * Transform the \Binds entity
     * @param Binds $model
     *
     * @return array
     */
    public function transform(Binds $model)
    {
        $config = json_decode($model->config, true);
        $config['bind_password'] = Crypt::decryptString($config['bind_password']);
        
        return [
            'id' => (int) $model->id,
            'type' => $model->getModelName(),
            'attributes' => [
                "bind_title" => $model->bind_title,
                "config" => $config
            ]
        ];
    }
}
