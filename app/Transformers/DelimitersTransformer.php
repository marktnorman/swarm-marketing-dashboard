<?php

namespace App\Transformers;

use Illuminate\Support\Facades\Lang;
use League\Fractal\TransformerAbstract;
use App\Models\Delimiters;

/**
 * Class DelimitersTransformer
 * @package namespace App\Transformers;
 */
class DelimitersTransformer extends TransformerAbstract
{

    /**
     * Transform the \Delimiters entity
     * @param Delimiters $model
     *
     * @return array
     */
    public function transform(Delimiters $model)
    {
        $data = array_only($model->toArray(), $model->getFillable());
        return [
            'id'         => (int) $model->id,
            'type' => $model->getModelName(),
            'attributes' => $data
        ];
    }
}
