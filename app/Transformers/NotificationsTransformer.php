<?php


namespace App\Transformers;



use League\Fractal\TransformerAbstract;

/**
 * Class NotificationsTransformer
 * @package App\Transformers
 */
class NotificationsTransformer extends TransformerAbstract
{
    /**
     * @param $notifications
     * @return array
     */
    public function transform($notifications)
    {
        $data = array_add($notifications->data, 'read_at', $notifications->read_at);
        return [
            'id' => $notifications->id,
            'type' => 'Notification',
            'attributes' => $data
        ];
    }

}