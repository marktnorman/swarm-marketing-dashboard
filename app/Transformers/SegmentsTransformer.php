<?php

namespace App\Transformers;

use Illuminate\Support\Facades\Lang;
use League\Fractal\TransformerAbstract;
use App\Models\Segments;
use Carbon\Carbon;

/**
 * Class SegmentsTransformer
 * @package namespace App\Transformers;
 */
class SegmentsTransformer extends TransformerAbstract
{

    /**
     * Transform the \Segments entity
     * @param Segments $model
     *
     * @return array
     */
    public function transform(Segments $model)
    {

        $data = array_only($model->toArray(), $model->getFillable());
        $data['delimiter_type_name'] = $model->delimiter->type;
        $data['updated_at'] = Carbon::parse($data['updated_at'])->toRssString();

        return [
            'id' => (int) $model->id,
            'type' => $model->getModelName(),
            'attributes' => $data
        ];
    }
}
