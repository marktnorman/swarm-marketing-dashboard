<?php

namespace App\Console\Commands;

use App\Models\Campaign;
use Illuminate\Console\Command;
use App\Jobs\CampaignSendJobs;
use App\Contracts\Facades\SwarmLog;

class CampaignMessageWorkGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campaign:generate-work';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'When this command runs it grabs the available campaign at the time and generates jobs with msisdns';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $current_time = date('Y-m-d H:i:s');
        $campaigns = Campaign::select('id')->where([['send_date', '<=', $current_time],['status', '=', Campaign::SCHEDULED],])->get();


        foreach ($campaigns as $campaign_id) {
            $campaign = Campaign::where([['id', '=', $campaign_id->id], ['status', '=', Campaign::SCHEDULED]])->firstOrFail();
            $campaign->status = Campaign::LOCKED;
            $campaign->save();

            // DISPATCH JOB
            $data =["id" => $campaign->id,
                    "updated_at" => $campaign->updated_at];

            CampaignSendJobs::dispatch($data)->onQueue('jobs');
            SwarmLog::info('campaigns', "Created Campaign Job: " . $campaign->id);
        }
    }
}
