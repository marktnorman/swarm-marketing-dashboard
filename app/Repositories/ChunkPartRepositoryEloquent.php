<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Interfaces\chunkPartRepository;
use App\Models\ChunkPart;
use App\Validators\ChunkPartValidator;

/**
 * Class ChunkPartRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ChunkPartRepositoryEloquent extends BaseRepository implements ChunkPartRepository
{
    protected $fieldSearchable = [];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ChunkPart::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
