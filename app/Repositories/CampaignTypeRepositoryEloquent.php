<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Interfaces\CampaignTypeRepository;
use App\Models\CampaignType;
use App\Validators\CampaignTypeValidator;

/**
 * Class CampaignTypeRepositoryEloquent
 * @package namespace App\Repositories;
 */
class CampaignTypeRepositoryEloquent extends BaseRepository implements CampaignTypeRepository
{
    protected $fieldSearchable = [];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CampaignType::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return CampaignTypeValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
