<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Interfaces\DelimitersRepository;
use App\Models\Delimiters;
use App\Validators\DelimitersValidator;

/**
 * Class DelimitersRepositoryEloquent
 * @package namespace App\Repositories;
 */
class DelimitersRepositoryEloquent extends BaseRepository implements DelimitersRepository
{
    protected $fieldSearchable = [];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Delimiters::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return DelimitersValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
