<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Interfaces\SegmentsRepository;
use App\Models\Segments;
use App\Validators\SegmentsValidator;

/**
 * Class SegmentsRepositoryEloquent
 * @package namespace App\Repositories;
 */
class SegmentsRepositoryEloquent extends BaseRepository implements SegmentsRepository
{
    protected $fieldSearchable = [
        'title' => 'like'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Segments::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return SegmentsValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
