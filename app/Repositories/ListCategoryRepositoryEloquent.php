<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Interfaces\listCategoryRepository;
use App\Models\ListCategory;
use App\Validators\ListCategoryValidator;

/**
 * Class ListCategoryRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ListCategoryRepositoryEloquent extends BaseRepository implements ListCategoryRepository
{
    protected $fieldSearchable = [];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ListCategory::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
