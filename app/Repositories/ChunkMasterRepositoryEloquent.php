<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Interfaces\chunkMasterRepository;
use App\Models\ChunkMaster;
use App\Validators\ChunkMasterValidator;

/**
 * Class ChunkMasterRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ChunkMasterRepositoryEloquent extends BaseRepository implements ChunkMasterRepository
{
    protected $fieldSearchable = [];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ChunkMaster::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
