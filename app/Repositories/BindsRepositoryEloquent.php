<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Interfaces\BindsRepository;
use App\Models\Binds;
use App\Validators\BindsValidator;

/**
 * Class BindsRepositoryEloquent
 * @package namespace App\Repositories;
 */
class BindsRepositoryEloquent extends BaseRepository implements BindsRepository
{
    protected $fieldSearchable = [
        'bind_title' => 'like',
        'config' => 'like'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Binds::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return BindsValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
