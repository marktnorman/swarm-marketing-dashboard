<?php

namespace App\Jobs;

use App\Models\ChunkMaster;
use App\Models\Segments;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Contracts\Facades\SwarmLog;
use Illuminate\Support\Facades\Mail;

class SegmentsStoreNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var
     */
    private $chunkMaster;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($chunk_master_id)
    {
        $this->chunkMaster = ChunkMaster::findOrFail($chunk_master_id);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        SwarmLog::info("segments",  "Chunk: Sending notification for Chunk Master: " . $this->chunkMaster->id);
        //Consolidate data from all parts

        $parts = $this->chunkMaster->chunkParts;
        $segment = Segments::findOrFail($this->chunkMaster->linked_id);
        $consolidatedData = [
            "counter"=> [
                "failed" => 0,
                "success" => 0,
                "duplicate" => 0,
                "new" => 0,
                "max" => json_decode($parts->first()->data)->counter->max
            ],
            "errors" => "",
            "exception" => "",
            "url" => json_decode($parts->first()->data)->url
        ];

        foreach ($parts as $part) {
            $data = json_decode($part->data);
            $consolidatedData['counter']['failed'] += $data->counter->failed;
            $consolidatedData['counter']['success'] += $data->counter->success;
            $consolidatedData['counter']['new'] += $data->counter->new;
            $consolidatedData['errors'] .= $data->errors;
            $consolidatedData['exception'] .= isset($data->exception) ? $data->exception: "";
        }


        //Send off the actual notifcation
        // Construct mail to editor
        $mail_data = [
            'email' => $this->chunkMaster->user->email,
            'first_name' => $this->chunkMaster->user->name,
            'from' => 'no-reply@hyvemobile.co.za',
            'from_name' => 'Administrator'];

        if($consolidatedData['exception'] != "")
        {
            $segment->stored = Segments::FAILED;
            $segment->save();
            $mail_data['body'] = 'Please note that the following segment has hit an exception while trying to upload - ' . $segment->title .
                "\n\nPlease see detailed information on the fault below" .
                "\nStart Time: " . $segment->created_at .
                "\nFinish Time: " . $segment->updated_at .
                "\nTotal: " . $consolidatedData['counter']['max'] .
                "\nSuccessful: " . $consolidatedData['counter']['success'] .
                "\nNew Msisdn's: " . $consolidatedData['counter']['new'] .
                "\nFailed: " . $consolidatedData['counter']['failed'] .
                "\nErrors: " . $consolidatedData['errors'];
            Mail::send([], $mail_data, function ($message) use ($mail_data) {
                $message->to($mail_data['email'])->from($mail_data['from'], $mail_data['first_name'])
                    ->subject('Swarm: Segment has failed to upload 💥')
                    ->setBody($mail_data['body']);
            });

            // Email off the Exception messages to a Dev for better visibility of the issue
            $mail_data['body'] .= "\nException " . $consolidatedData['exception'];
            Mail::send([], $mail_data, function ($message) use ($mail_data) {
                $message->to(env("MAIL_ADMIN", 'john@hyvemobile.co.za'))->from($mail_data['from'], $mail_data['first_name'])
                    ->subject('Swarm: Segment has failed to upload 💥')
                    ->setBody($mail_data['body']);
            });




        }
        else if($consolidatedData['errors'] > 0)
        {
            $segment->stored = Segments::STORED;
            $segment->save();
            $mail_data['body'] = 'Please note that the following segment had some msisdns fail validation - ' . $segment->title .
                "\n\nPlease see detailed information on the warning below" .
                "\nStart Time: " . $segment->created_at .
                "\nFinish Time: " . $segment->updated_at .
                "\nTotal: " . $consolidatedData['counter']['max'] .
                "\nSuccessful: " . $consolidatedData['counter']['success'] .
                "\nNew Msisdn's: " . $consolidatedData['counter']['new'] .
                "\nFailed: " . $consolidatedData['counter']['failed'] .
                "\nErrors: " . $consolidatedData['errors'];

            Mail::send([], $mail_data, function ($message) use ($mail_data) {
                $message->to($mail_data['email'])->from($mail_data['from'], $mail_data['first_name'])
                    ->subject('Swarm: Segment uploaded successfully, but with some validation errors ⚠️')
                    ->setBody($mail_data['body']);
            });
        }

        else
        {
            $segment->stored = Segments::STORED;
            $segment->save();
            $mail_data['body'] = 'The following segment has successfully passed through validation! ' . $segment->title .
                "\n\nPlease see detailed information" .
                "\nStart Time: " . $segment->created_at .
                "\nFinish Time: " . $segment->updated_at .
                "\nTotal: " . $consolidatedData['counter']['max'] .
                "\nSuccessful: " . $consolidatedData['counter']['success'] .
                "\nNew Msisdn's: " . $consolidatedData['counter']['new'];

            Mail::send([], $mail_data, function ($message) use ($mail_data) {
                $message->to($mail_data['email'])->from($mail_data['from'], $mail_data['first_name'])
                    ->subject('Swarm: Segment uploaded successfully! 🚀 ')
                    ->setBody($mail_data['body']);
            });
        }
    }
}
