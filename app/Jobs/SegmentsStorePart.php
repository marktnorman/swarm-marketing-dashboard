<?php

namespace App\Jobs;

use App\Models\ChunkPart;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Msisdn;
use App\Models\MsisdnSegment;
use App\Models\Segments;
use League\Csv\Reader;
use Illuminate\Support\Facades\Storage;
use App\Contracts\Facades\SwarmLog;
use libphonenumber\PhoneNumberUtil;
use libphonenumber\PhoneNumberFormat;

/**
 * Class SegmentsStorePart
 * @package App\Jobs
 */
class SegmentsStorePart implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var
     */
    private $data;
    private $segment;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->segment = Segments::findOrFail($data['segment_id']);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        // Get the file
        $file = file_get_contents(Storage::disk('s3')->url($this->data['url']));

        // Import the file into the reader
        $reader = Reader::createFromString($file);

        // Set the delimiter
        $reader->setDelimiter($this->data['delimiter']);

        //Get the CSV Data in
        $file_contents = $reader->fetchAll();

        if(strtolower($file_contents[$this->data['counter']['current']][0]) == "msisdn"){
            $this->data['counter']['current']++;
            $this->data['counter']['progress']++;
        }

        try {
            // Read the data inside of the CSV and go through the msisdn insert / update process
            for($shortEnd = $this->data['counter']['max'];$this->data['counter']['current'] < $shortEnd;$this->data['counter']['current']++)
            {
                $row = $file_contents[$this->data['counter']['current']];

                $phoneNumberUtil = PhoneNumberUtil::getInstance();
                $phoneNumberObject = $phoneNumberUtil->parse($row[0], $row[1]);

                // Getting msisdn with country code attached
                $msisdn_num = $phoneNumberUtil->format($phoneNumberObject, PhoneNumberFormat::INTERNATIONAL);

                // Removing spaces and + sign
                $msisdn_num = str_replace(' ', '', $msisdn_num);
                $msisdn_num = str_replace('+', '', $msisdn_num);

                // Use Google's phone number library to check if the supplied number is legit
                if ($phoneNumberUtil->isPossibleNumber($phoneNumberObject) && $phoneNumberUtil->isValidNumber($phoneNumberObject)) {

                    // See if the number exists
                    $msisdn = Msisdn::where('msisdn', '=', $msisdn_num)->first();

                    if ($msisdn) {
                        $msisdn->country_code = $row[1];
                        $msisdn->mno_id = (int)$row[2];
                        $msisdn->channel = (int)$row[3];
                        $msisdn->last_active = date('Y-m-d H:i:s');
                        $msisdn->save();

                        // Set the new MSISDN ID where it will be used below
                    } else {
                        $msisdn = Msisdn::create([
                            'msisdn' => $msisdn_num,
                            'country_code' => $row[1],
                            'mno_id' => (int)$row[2],
                            'channel' => (int)$row[3],
                            'last_active' => date('Y-m-d H:i:s')
                        ]);
                        $this->data['counter']['new']++;
                    }

                    // Here we save every MSISDN assigned to it's segment ID, msisdn's can have multiple segments
                    MsisdnSegment::create([
                        'msisdn_id' => $msisdn->id,
                        'segment_id' => $this->data['segment_id'],
                        'msisdn' => $msisdn->msisdn
                    ]);
                    $this->data['counter']['success']++;
                }
                else{
                    $this->data['errors'] .= "\nMsisdn failed validation:" . $msisdn_num;
                    $this->data['counter']['failed']++;

                }
                $this->data['counter']['progress']++;
            }

            $dataToBeLogged = "Segment ID:" . $this->data['segment_id'] . " |  Part: " . $this->data['counter']['chunk'] . " imported."
                . " Successful: ". $this->data['counter']['success'];

            if($this->data['counter']['failed'] > 0){
                $dataToBeLogged .= " | Errors: " . $this->data['counter']['failed'] . "\nErrors:\n" . $this->data['errors'];
                SwarmLog::warning("segments", "Part: Validation issues: " . $dataToBeLogged);
                $this->logPartData();
            }
            else{
                SwarmLog::info("segments", "Part: Validated Successfully! " . $dataToBeLogged);
                $this->logPartData();
            }

        }catch(\Exception $e) {
            $this->data['exception'] = $e->getMessage();

            $dataToBeLogged = "Segment : " . $this->segment->title . " imported."
                . " | Total: ". $this->data['counter']['max']
                . " | New MSISDN's: ". $this->data['counter']['new']
                . " | Progress: ". $this->data['counter']['progress']
                . " | Successful: ". $this->data['counter']['success']
                . " | Failed: " . $this->data['counter']['failed']
                . " | Errors: " . $this->data['errors']
                . " | Exception: " . $this->data['exception'];

            SwarmLog::error("segments", "Part: Exception: " . $dataToBeLogged);
            $this->logPartData();
        }
    }
    private function logPartData(){
        //Log info about this part
        $part = ChunkPart::create([
            "chunk_masters_id" => $this->data['chunk_master_id'],
            "part_num" => $this->data['counter']['chunk'],
            "data" => json_encode($this->data)
        ]);

        //Check if this is the last chunk to save data, if so, dispatch notification
        if($part->chunkMaster->chunkParts->count() >= $part->chunkMaster->parts){
            SegmentsStoreNotification::dispatch($this->data['chunk_master_id'])->onQueue('StoreSegments');
        }
    }
}
