<?php

namespace App\Jobs;

use App\Models\SmsMt;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Contracts\Facades\SwarmLog;

/**
 * Class ExecuteCampaign
 * @package App\Jobs
 */
class ExecuteCampaign implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var
     */
    private $campaign_job;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($campaign_job)
    {
        $this->campaign_job = $campaign_job;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $url = $_ENV["APP_URL"] . '/api/totalsend-delivery-report?mt_ref=' . $this->campaign_job['bind_reference'] . '&status=%d';

        // Case 1 has a from address
        if (isset($this->campaign_job['from']) && $this->campaign_job['from'] != null) {
            $vars = array(
                'action' => 'message_send',
                'username' => $this->campaign_job['username'],
                'password' => $this->campaign_job['password'],
                'to' => $this->campaign_job['msisdn'],
                'from' => $this->campaign_job['from'],
                'text' => $this->campaign_job['copy'],
                'report_url' => $url,
                'report_mask' => '31'
            );
            // Insert into SMS MT table
            SmsMt::create([
                'msisdn' => $this->campaign_job['msisdn'],
                'message' => $this->campaign_job['copy'],
                'from' => $this->campaign_job['from'],
                'campaign_id' => $this->campaign_job['campaign_id'],
                'bind_id' => $this->campaign_job['bind_id'],
                'bind_reference' => $this->campaign_job['bind_reference'],
                'status' => 0
            ]);
            // Case 2 doesn't have a from address
        } else {
            $vars = array(
                'action' => 'message_send',
                'username' => $this->campaign_job['username'],
                'password' => $this->campaign_job['password'],
                'to' => $this->campaign_job['msisdn'],
                'text' => $this->campaign_job['copy'],
                'report_url' => $url,
                'report_mask' => '31'
            );
            // Insert into SMS MT table
            SmsMt::create([
                'msisdn' => $this->campaign_job['msisdn'],
                'message' => $this->campaign_job['copy'],
                'campaign_id' => $this->campaign_job['campaign_id'],
                'bind_id' => $this->campaign_job['bind_id'],
                'bind_reference' => $this->campaign_job['bind_reference'],
                'status' => 0
            ]);
        }

        $querystring = http_build_query($vars);

        $url = $this->campaign_job['url'] . '?' . $querystring;


        $client = new Client();
        $response = $client->get($url);


        $log_message = "Code: " . $response->getStatusCode() .
                        " - Phrase: " . $response->getReasonPhrase() .
                        " - URL: " . $url .
                        " - Body: " . $response->getBody()->getContents();

        if($response->getStatusCode() == "200"){
            SwarmLog::info("jobs", $log_message);
        }
        else{
            SwarmLog::error("jobs", $log_message);
        }


    }
}
