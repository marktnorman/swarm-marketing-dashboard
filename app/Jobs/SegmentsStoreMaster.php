<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use League\Csv\Reader;
use App\Models\Segments;
use App\Models\ChunkMaster;
use Illuminate\Support\Facades\Storage;
use App\Contracts\Facades\SwarmLog;
use SplFileObject;

class SegmentsStoreMaster implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var
     */
    private $segment;
    private $data;
    private $chunkSize;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($segment_id, $chunkSize = 20000)
    {
        $this->segment = Segments::findOrFail($segment_id);
        $this->chunkSize = $chunkSize;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        SwarmLog::info("segments",  "Chunking Segment " . $this->segment->id . "...");
        // Get editor email which loaded the segment
        $editor = $this->segment->user;
        $s3_url = 'csv-files/current/' . $this->segment->file_name;

        if (Storage::disk('s3')->exists($s3_url)) {
            // Get the file
            $file = file_get_contents(Storage::disk('s3')->url($s3_url));

            // Import the file into the reader
            $reader = Reader::createFromString($file);
            // Set the delimiter
            $delimiter = $this->getFileDelimiter(Storage::disk('s3')->url($s3_url));
            $reader->setDelimiter($delimiter);

            //Get the CSV Data in
            $file_contents = $reader->fetchAll();
            //Create the ChunkMaster
            $ChunkMaster = ChunkMaster::create([
                "user_id" => $this->segment->user_id,
                "grouping" => "Segments",
                "linked_id" => $this->segment->id,
                "name" => $this->segment->title,
                "parts" => 0,
                "size" => $this->chunkSize,
            ]);

            //Start a counter (this is used for row counts/headings)
            $this->data['counter'] = [
                "progress" => 0,
                "new" => 0,
                "failed" => 0,
                "success" => 0,
                "current" => 0,
                "max" => count($file_contents),
                "chunk" => 0
            ];
            $this->data['errors'] = "";
            $this->data['segment_id'] = $this->segment->id;
            $this->data['delimiter'] = $delimiter;
            $this->data['url'] = $s3_url;
            $this->data['chunk_master_id'] = $ChunkMaster->id;


            $chunkCounter = 0;
            for($newData = $this->data;
                $this->data['counter']['current'] < $this->data['counter']['max'];
                $this->data['counter']['current'] += $this->chunkSize)
            {

                $chunkCounter++;
                if(($this->data['counter']['current'] + $this->chunkSize) < $this->data['counter']['max']){
                    $newData['counter']['current'] = $this->data['counter']['current'];
                    $newData['counter']['max'] = $this->data['counter']['current'] + $this->chunkSize;
                    $newData['counter']['chunk'] = $chunkCounter;
                    SegmentsStorePart::dispatch($newData)->onQueue('StoreSegments');
                }
                else{
                    $newData['counter']['current'] = $this->data['counter']['current'];
                    $newData['counter']['max'] = $this->data['counter']['max'];
                    $newData['counter']['chunk'] = $chunkCounter;
                    SegmentsStorePart::dispatch($newData)->onQueue('StoreSegments');
                    break;
                }
            }

            SwarmLog::info("segments", "Chunker: " . $chunkCounter . " chunks of up to " . $this->chunkSize . " lines were created for file: " . $this->segment->download_url . " which had " . $this->data['counter']['max'] . " lines");

            //Update the ChunkMaster with the count of the parts
            $ChunkMaster->parts = $chunkCounter;
            $ChunkMaster->save();
        }
        else{
            SwarmLog::error("segments", "Chunker: Unable to find csv on s3");
        }


    }

    function getFileDelimiter($file, $checkLines = 2){
        $file = new SplFileObject($file);
        $delimiters = array(
            ',',
            '\t',
            ';',
            '|',
            ':'
        );
        $results = array();
        $i = 0;
        while($file->valid() && $i <= $checkLines){
            $line = $file->fgets();
            foreach ($delimiters as $delimiter){
                $regExp = '/['.$delimiter.']/';
                $fields = preg_split($regExp, $line);
                if(count($fields) > 1){
                    if(!empty($results[$delimiter])){
                        $results[$delimiter]++;
                    } else {
                        $results[$delimiter] = 1;
                    }
                }
            }
            $i++;
        }
        $results = array_keys($results, max($results));
        return $results[0];
    }
}
