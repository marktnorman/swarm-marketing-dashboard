<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Binds;
use App\Models\Campaign;
use App\Models\MsisdnSegment;
use App\Models\MsisdnSegmentCampaign;
use App\Jobs\ExecuteCampaign;
use App\Contracts\Facades\SwarmLog;
use App\Transformers\BindsTransformer;

/**
 * Class CampaignSendJobs
 * @package App\Jobs
 */
class CampaignSendJobs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var
     */
    private $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $campaign = Campaign::where([
            ['id', '=', $this->data['id']],
            ['status', '=', Campaign::LOCKED],
            ['updated_at', '=', $this->data['updated_at']]
        ])->firstOrFail();
        SwarmLog::info('campaigns', "Executing Campaign Job :" . $campaign->id);

        $rows = MsisdnSegment::select('msisdn', 'msisdn_id')
            ->where('segment_id', '=', $campaign->segment_id)
            ->get()
            ->toArray();
        $bind =  (new BindsTransformer)->transform(Binds::find($campaign->bind_id));
        $counter = [
            "total" => count($rows),
            "success" => 0,
            "failed" => 0,
            "errors" => ""
        ];

        foreach ($rows as $row) {
            try{

                $reference = str_replace('.', '', uniqid(time(), true));

                $campaign_job = [
                    'msisdn' => $row['msisdn'],
                    'copy' => $campaign->copy,
                    'campaign_id' => $campaign->id,
                    'bind_id' => $campaign->bind_id,
                    'bind_reference' => $reference,
                    'username' => $bind['attributes']['config']['bind_username'],
                    'password' => $bind['attributes']['config']['bind_password'],
                    'url' => $bind['attributes']['config']['api_endpoint_url']
                ];

                if (isset($campaign->from) && $campaign->from != null) {
                    $campaign_job['from'] = $campaign->from;
                }

                // Create new MsisdnSegmentCampaign
                MsisdnSegmentCampaign::create([
                    'msisdn_id' => $row['msisdn_id'],
                    'segment_id' => $campaign->segment_id,
                    'campaign_id' => $campaign->id,
                ]);

                ExecuteCampaign::dispatch($campaign_job)->onQueue('jobs');
                $counter['success']++;
            }
            catch(\Exception $e){
                $counter['failed']++;
                $counter['errors'] .= "\nMsisdn: " . $row['msisdn'] . " | Campaign:" . $campaign->id;
                $counter['errors'] .= "\n" . $e->getMessage();
            }
        }

        $campaign->status = Campaign::EXECUTED;
        $campaign->save();


        if($counter['failed'] > 0 ){
            SwarmLog::warning('campaigns', "Campaign :" . $campaign->id
                . " | Total jobs: " . $counter['total']
                . " | Success jobs: " . $counter['success']
                . " | Failed jobs: " . $counter['failed']
                . " | Errors: " . $counter['errors']
                . " | Failed jobs: " . $counter['failed']
            );
        }
        else{
            SwarmLog::info('campaigns', "Campaign :" . $campaign->id
                . " | Total jobs: " . $counter['total']
                . " | Success jobs: " . $counter['success']
                . " | Failed jobs: " . $counter['failed']
            );
        }
    }
}
