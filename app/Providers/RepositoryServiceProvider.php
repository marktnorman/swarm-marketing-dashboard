<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class RepositoryServiceProvider
 * @package App\Providers
 */
class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Interfaces\UserRepository::class, \App\Repositories\UserRepositoryEloquent::class);
        $this->app->bind(\App\Interfaces\RoleRepository::class, \App\Repositories\RoleRepositoryEloquent::class);
        $this->app->bind(\App\Interfaces\PermissionRepository::class, \App\Repositories\PermissionRepositoryEloquent::class);
        $this->app->bind(\App\Interfaces\BindsRepository::class, \App\Repositories\BindsRepositoryEloquent::class);
        $this->app->bind(\App\Interfaces\SegmentsRepository::class, \App\Repositories\SegmentsRepositoryEloquent::class);
        $this->app->bind(\App\Interfaces\UploadsRepository::class, \App\Repositories\UploadsRepositoryEloquent::class);
        $this->app->bind(\App\Interfaces\SegmentsStoresRepository::class, \App\Repositories\SegmentsStoresRepositoryEloquent::class);
        $this->app->bind(\App\Interfaces\DelimitersRepository::class, \App\Repositories\DelimitersRepositoryEloquent::class);
        $this->app->bind(\App\Interfaces\CampaignRepository::class, \App\Repositories\CampaignRepositoryEloquent::class);
        $this->app->bind(\App\Interfaces\CampaignTypeRepository::class, \App\Repositories\CampaignTypeRepositoryEloquent::class);
        $this->app->bind(\App\Interfaces\ChunkMasterRepository::class, \App\Repositories\ChunkMasterRepositoryEloquent::class);
        $this->app->bind(\App\Interfaces\ChunkPartRepository::class, \App\Repositories\ChunkPartRepositoryEloquent::class);
        $this->app->bind(\App\Interfaces\ListCategoryRepository::class, \App\Repositories\ListCategoryRepositoryEloquent::class);
        //:end-bindings:
    }
}
