<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


/**
 * Class PasswordChanged
 * @package App\Mail
 */
class PasswordChanged extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $request;

    /**
     * PasswordChanged constructor.
     * @param $user
     * @param $request
     */
    public function __construct($user, $request)
    {
        $this->user = $user;
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.passwords.changed')->with([
            'userName' => $this->user->first_name . '&nbsp;' . $this->user->last_name,
            'resetPasswordLink' => 'http://ondash-trove.hyve.tech'
        ]);
    }
}
