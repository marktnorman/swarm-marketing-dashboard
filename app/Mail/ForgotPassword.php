<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


/**
 * Class ForgotPassword
 * @package App\Mail
 */
class ForgotPassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var
     */
    protected $user;
    /**
     * @var
     */
    protected $token;


    /**
     * ForgotPassword constructor.
     * @param $token
     * @param $user
     */
    public function __construct($token, $user)
    {
        $this->token = $token;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Reset Your Password')
            ->view('emails.passwords.forgot')->with([
                'userName' => $this->user->first_name . '&nbsp;' . $this->user->last_name,
                'resetPasswordLink' => env('APP_URL') . '?email=' . $this->user->email . '&token=' . $this->token,
            ]);
    }
}
