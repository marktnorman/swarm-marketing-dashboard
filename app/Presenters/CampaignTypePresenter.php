<?php

namespace App\Presenters;

use App\Transformers\CampaignTypeTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class CampaignTypePresenter
 *
 * @package namespace App\Presenters;
 */
class CampaignTypePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new CampaignTypeTransformer();
    }
}
