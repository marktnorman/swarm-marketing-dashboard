<?php

namespace App\Presenters;

use App\Transformers\DelimitersTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class DelimitersPresenter
 *
 * @package namespace App\Presenters;
 */
class DelimitersPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new DelimitersTransformer();
    }
}
