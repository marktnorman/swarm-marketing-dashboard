<?php

namespace App\Presenters;

use App\Transformers\UploadsTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class UploadsPresenter
 *
 * @package namespace App\Presenters;
 */
class UploadsPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new UploadsTransformer();
    }
}
