<?php

namespace App\Presenters;

use App\Transformers\BindsTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class BindsPresenter
 *
 * @package namespace App\Presenters;
 */
class BindsPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new BindsTransformer();
    }
}
