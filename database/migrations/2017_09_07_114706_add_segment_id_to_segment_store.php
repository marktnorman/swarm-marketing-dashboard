<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSegmentIdToSegmentStore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('segments_store', function($table) {
            $table->unsignedInteger('segment_id')->nullable()->after('segment_title');
            $table->foreign('segment_id')->references('id')->on('segments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('segments_store', function($table) {
            $table->dropForeign('segment_id');
            $table->dropColumn('segment_id');
        });
    }
}