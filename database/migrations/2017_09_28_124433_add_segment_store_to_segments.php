<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSegmentStoreToSegments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('segments', function($table) {
            $table->string('file_name', '255')->after('title');
            $table->integer('user_id')->unsigned()->after('type');
            $table->tinyInteger('locked')->default('0')->after('stored');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('segments', function($table) {
            $table->dropColumn('user_id');
            $table->dropColumn('file_name');
            $table->dropColumn('locked');
        });
    }
}
