<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMsisdnSegmentCampaign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msisdn_segment_campaign', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('msisdn_id')->nullable();
            $table->unsignedInteger('segment_id')->nullable();
            $table->unsignedInteger('campaign_id')->nullable();

            $table->timestamps();
        });

        Schema::table('msisdn_segment_campaign', function ($table) {
            $table->foreign('msisdn_id')->references('id')->on('msisdns');
            $table->foreign('segment_id')->references('id')->on('segments');
            $table->foreign('campaign_id')->references('id')->on('campaigns');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('msisdn_segment_campaign', function ($table) {
            $table->dropForeign('msisdn_id');
            $table->dropForeign('segment_id');
            $table->dropForeign('campaign_id');
        });

        Schema::dropIfExists('msisdn_segment_campaign');
    }
}
