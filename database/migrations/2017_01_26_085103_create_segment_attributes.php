<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSegmentAttributes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('segment_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('segment_id')->nullable();
            $table->string('key', 10);
            $table->string('value', 10);
            $table->timestamps();
        });

        Schema::table('segment_attributes', function ($table) {
            $table->foreign('segment_id')->references('id')->on('segments');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('segment_attributes', function ($table) {
            $table->dropForeign('segment_id');
        });

        Schema::dropIfExists('segment_attributes');
    }
}
