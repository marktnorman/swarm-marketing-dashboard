<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMsisdns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msisdns', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('msisdn');
            $table->string('country_code', 5);
            $table->unsignedInteger('mno_id');
            $table->dateTime('last_active');
            $table->unsignedInteger('account_type')->nullable();
            $table->unsignedInteger('device_type')->nullable();
            $table->unsignedInteger('channel');
            $table->timestamps();
        });

        Schema::table('msisdns', function ($table) {
            $table->foreign('mno_id')->references('id')->on('mnos');
            $table->foreign('account_type')->references('id')->on('account_type');
            $table->foreign('device_type')->references('id')->on('device_type');
            $table->foreign('channel')->references('id')->on('channels');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('msisdns', function ($table) {
            $table->dropForeign('mno_id');
            $table->dropForeign('account_type');
            $table->dropForeign('device_type');
            $table->dropForeign('channel');
        });

        Schema::dropIfExists('msisdns');
    }
}
