<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveSegmentStoreforeign2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('segments_store');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('segments_store', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('file_name', '255');
            $table->string('segment_title', '50');
            $table->string('user_email', '255');
            $table->unsignedInteger('delimiter_type')->nullable();
            $table->tinyInteger('locked')->default('0');
            $table->tinyInteger('stored')->default('0');
            $table->timestamps();
        });
    }
}
