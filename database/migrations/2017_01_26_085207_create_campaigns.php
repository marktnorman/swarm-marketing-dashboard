<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaigns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 50);
            $table->string('copy', 255);
            $table->dateTime('send_date');
            $table->unsignedInteger('segment_id')->nullable();
            $table->unsignedInteger('bind_id')->nullable();
            $table->unsignedInteger('campaign_type')->nullable();
            $table->unsignedInteger('service_id')->nullable();
            $table->tinyInteger('campaign_sent')->default('0');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('campaigns', function ($table) {
            $table->foreign('segment_id')->references('id')->on('segments');
            $table->foreign('bind_id')->references('id')->on('binds');
            $table->foreign('campaign_type')->references('id')->on('campaign_type');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaigns', function ($table) {
            $table->dropForeign('segment_id');
            $table->dropForeign('bind_id');
            $table->dropForeign('campaign_type');
        });

        Schema::dropIfExists('campaigns');
    }
}
