<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChunkPartsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('chunk_parts', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('chunk_masters_id')->nullable();
            $table->integer('part_num');
            $table->text('data');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('chunk_parts');
	}

}
