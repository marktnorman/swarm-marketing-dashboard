<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMsisdnSegments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msisdn_segments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('msisdn_id');
            $table->unsignedInteger('segment_id');
            $table->unsignedbigInteger('msisdn');
            $table->timestamps();
        });

        Schema::table('msisdn_segments', function ($table) {
            $table->foreign('msisdn_id')->references('id')->on('msisdns');
            $table->foreign('segment_id')->references('id')->on('segments');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('msisdn_segments', function ($table) {
            $table->dropForeign('msisdn_id');
            $table->dropForeign('segment_id');
        });

        Schema::dropIfExists('msisdn_segments');
    }
}
