<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SmsMo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_mo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('message', 100);
            $table->unsignedBigInteger('msisdn');
            $table->unsignedInteger('campaign_id')->nullable();
            $table->timestamps();
        });

        Schema::table('sms_mo', function ($table) {
            $table->foreign('campaign_id')->references('id')->on('campaigns');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sms_mo', function ($table) {
            $table->dropForeign('campaign_id');
        });

        Schema::dropIfExists('channels');
    }
}
