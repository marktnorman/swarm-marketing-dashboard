<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMsisdnsLastActiveType3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::table('msisdns')
            ->update([
                'last_active' => DB::raw("`last_active_temp`")
            ]);



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('msisdns')
            ->update([
                'last_active' => DB::raw("NULL")
            ]);
    }
}
