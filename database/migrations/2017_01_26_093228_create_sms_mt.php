<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsMt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_mt', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedbigInteger('msisdn')->nullable();
            $table->string('message', 255);
            $table->unsignedInteger('campaign_id')->nullable();
            $table->unsignedInteger('bind_id')->nullable();
            $table->string('bind_reference', 255);
            $table->unsignedInteger('status')->nullable();
            $table->timestamps();
        });

        Schema::table('sms_mt', function ($table) {
            $table->foreign('campaign_id')->references('id')->on('campaigns');
            $table->foreign('bind_id')->references('id')->on('binds');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sms_mt', function ($table) {
            $table->dropForeign('campaign_id');
            $table->dropForeign('bind_id');
        });

        Schema::dropIfExists('sms_mt');
    }
}
