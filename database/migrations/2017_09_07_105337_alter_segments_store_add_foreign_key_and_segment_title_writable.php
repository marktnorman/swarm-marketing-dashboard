<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSegmentsStoreAddForeignKeyAndSegmentTitleWritable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('segments_store', function($table) {
            $table->string('segment_title_writable')->after('segment_title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('segments_store', function($table) {
            $table->dropColumn('segment_title_writable');
        });
    }
}
