<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveSegmentStoreforeign3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('segments')
            ->where('stored', "=", 1)
            ->update([
                'stored' => 5
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('segments')
            ->where('stored', "=", 5)
            ->update([
                'stored' => 1
            ]);
    }
}
