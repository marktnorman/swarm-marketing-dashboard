<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveSegmentStoreforeign0 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $segments_store = DB::table('segments_store')->get();

        foreach ($segments_store as $stores) {
            $user_id = DB::table('users')->select('id')->where('email', '=', $stores->user_email)->first()->id;
            DB::table('segments')
                ->where('id', $stores->segment_id)
                ->update([
                    'file_name' => $stores->file_name,
                    'user_id' => $user_id,
                    'locked' => $stores->locked
                ]);
        }

        DB::table('segments')
            ->where('user_id', 0)
            ->update([
                'user_id' => 1
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('segments')
            ->update([
                'file_name' => "Migration Rollback",
                'user_id' => 0,
                'locked' => 0
            ]);
    }
}
