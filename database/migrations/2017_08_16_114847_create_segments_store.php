<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSegmentsStore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('segments_store', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('file_name', '255');
            $table->string('segment_title', '255');
            $table->string('user_email', '255');
            $table->unsignedInteger('delimiter_type')->nullable();
            $table->tinyInteger('locked')->default('0');
            $table->tinyInteger('stored')->default('0');
            $table->timestamps();
        });

        Schema::table('segments_store', function ($table) {
            $table->foreign('delimiter_type')->references('id')->on('delimiters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('segments_store', function ($table) {
            $table->dropForeign('delimiter_type');
        });

        Schema::dropIfExists('segments_store');
    }
}