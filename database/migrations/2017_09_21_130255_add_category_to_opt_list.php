<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryToOptList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('opt_list', function($table) {
            $table->unsignedInteger('category')->nullable()->after('list');
        });

        Schema::table('opt_list', function($table) {
            $table->foreign('category')->references('id')->on('list_category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('opt_list', function($table) {
            $table->dropForeign('category');
        });

        Schema::table('opt_list', function($table) {
            $table->dropColumn('category');
        });
    }
}
