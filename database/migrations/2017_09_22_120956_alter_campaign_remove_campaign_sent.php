<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCampaignRemoveCampaignSent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaigns', function($table) {
            $table->dropColumn('campaign_sent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaigns', function($table) {
            $table->smallInteger('campaign_sent')->before('status')->default(0);
        });

        DB::table('campaigns')
            ->update([
                'campaign_sent' => DB::raw("`status`")
            ]);
    }
}
