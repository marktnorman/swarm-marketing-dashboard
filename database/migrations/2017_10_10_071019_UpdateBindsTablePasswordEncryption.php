<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBindsTablePasswordEncryption extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $binds = DB::table('binds')->get();
        foreach ($binds as $bind){
            $config = json_decode($bind->config, true);
            $config['bind_password'] = \Illuminate\Support\Facades\Crypt::encryptString($config['bind_password']);
            DB::table('binds')
                ->where('id', $bind->id)
                ->update([
                    'config' => json_encode($config)
                ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $binds = DB::table('binds')->get();
        foreach ($binds as $bind){
            $config = json_decode($bind->config, true);
            $config['bind_password'] = \Illuminate\Support\Facades\Crypt::decryptString($config['bind_password']);
            DB::table('binds')
                ->where('id', $bind->id)
                ->update([
                    'config' => json_encode($config)
                ]);
        }
    }
}