<?php

use Illuminate\Database\Seeder;

class DelimitersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $delimiters = array('semi-colon', 'comma');

        foreach ($delimiters as $delimiter) :
            DB::table('delimiters')->insert([
                'type' => $delimiter,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        endforeach;
    }
}