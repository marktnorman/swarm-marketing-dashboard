<?php

use Illuminate\Database\Seeder;

class MnosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mnos = array(
            'Vodacom',
            'MTN',
            'Cell C',
            'Telkom Mobile',
            'MTN IMI',
            'MTN SDP',
            'Zamtel',
            'MTN SDP NG',
            'AWCC',
            'Econet',
            'Integrat',
            'MTN Play IMI',
            'MTC',
            'Nexttel',
            'GlobePh',
            'Etisalat',
            'Airtel',
        );

        foreach ($mnos as $mno) :
            DB::table('mnos')->insert([
                'name' => $mno,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        endforeach;
    }
}
