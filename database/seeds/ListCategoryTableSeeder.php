<?php

use Illuminate\Database\Seeder;

class ListCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = array('opt_variations', 'campaign_status', 'segment_status');

        foreach ($categories as $c) :
            DB::table('list_category')->insert([
                'list' => $c,
                'updated_at' => \Carbon\Carbon::now(),
                'created_at' => \Carbon\Carbon::now()
            ]);
        endforeach;
    }
}
