<?php

use Illuminate\Database\Seeder;

class AccountTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $account_types = array('prepaid', 'postpaid', 'hybrid');

        foreach ($account_types as $account_type) :
            DB::table('account_type')->insert([
                'name' => $account_type,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        endforeach;
    }
}
