<?php

use Illuminate\Database\Seeder;

class ChannelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $channels = array('ussd', 'wap_doi', 'wap_sms', 'mo', 'wap', 'ni_ussd', 'ni_ussd_sms', 'ni_ussd_wap', 'ni_ussd_wap_sms');

        foreach ($channels as $channel) :
            DB::table('channels')->insert([
                'name' => $channel,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        endforeach;
    }
}
