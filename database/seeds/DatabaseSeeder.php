<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ChannelsTableSeeder::class);
        $this->call(DeviceTypeTableSeeder::class);
        $this->call(AccountTypeTableSeeder::class);
        $this->call(CampaignTypeTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(MnosTableSeeder::class);
        $this->call(DelimitersTableSeeder::class);
        $this->call(BindsTableSeeder::class);
        $this->call(ListCategoryTableSeeder::class);
        $this->call(OptListTableSeeder::class);
    }
}