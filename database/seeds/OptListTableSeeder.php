<?php

use Illuminate\Database\Seeder;

class OptListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $status = array(
            array('2' => 'Scheduled'),
            array('2' => 'In progress'),
            array('2' => 'Executed'),
            array('3' => 'In progress'),
            array('3' => 'Stored'),
            array('3' => 'Failed'),
            array('1' => 'yes'),
            array('1' => 'eys'),
            array('1' => 'yse'),
            array('1' => 'ye'),
            array('1' => 'yees'),
            array('1' => 'yeees'),
            array('1' => 'yyes'),
            array('1' => 'yess'),
            array('1' => 'ys'),
            array('1' => 'y')
        );

        foreach ($status as $option) {
            foreach ($option as $key => $value) {
                DB::table('opt_list')->insert([
                    'list' => $value,
                    'category' => $key,
                    'updated_at' => \Carbon\Carbon::now(),
                    'created_at' => \Carbon\Carbon::now()
                ]);
            }
        }

    }
}
