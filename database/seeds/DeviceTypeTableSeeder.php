<?php

use Illuminate\Database\Seeder;

class DeviceTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $device_types = array('smart', 'feature');

        foreach ($device_types as $device_type) :
            DB::table('device_type')->insert([
                'name' => $device_type,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        endforeach;
    }
}
