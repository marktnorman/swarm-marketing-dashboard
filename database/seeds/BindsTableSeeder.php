<?php

use Illuminate\Database\Seeder;

class BindsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = \Illuminate\Support\Facades\Crypt::encryptString('qj8j5QXeTQ8');
        DB::table('binds')->insert([
            'bind_title' => 'CellC',
            'config' => '{"bind_username":"hyvemobile","bind_password":' . $password . ',"api_endpoint_url":"http:\/\/hyveapi.totalsend.com\/json"}'
        ]);
    }
}
