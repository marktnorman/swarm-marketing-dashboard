<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Mark Norman',
                'email' => 'mark@hyvemobile.co.za',
                'password' => bcrypt('weiyfg864o7fvywilefv'),
                'docker' => '1'
            ],
            [
                'name' => 'Dillon Kin',
                'email' => 'dillon@hyvemobile.co.za',
                'password' => bcrypt('iysev8w7egvfiubsseff'),
                'docker' => '1'
            ],
            [
                'name' => 'Tyler Reed',
                'email' => 'tyler@hyvemobile.co.za',
                'password' => bcrypt('w8yoevfo8weg78guffswe'),
                'docker' => '1'
            ],
            [
                'name' => 'Michelle Vieira',
                'email' => 'michelle@hyvemobile.co.za',
                'password' => bcrypt('oubgrso9we9uhelsleeff'),
                'docker' => '1'
            ],
        ]);
    }
}
