<?php

use Illuminate\Database\Seeder;

class CampaignTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $campaign_types = array('sms');

        foreach ($campaign_types as $campaign_type) :
            DB::table('campaign_type')->insert([
                'name' => $campaign_type,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        endforeach;
    }
}
