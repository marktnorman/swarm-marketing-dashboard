<?php

use Illuminate\Http\Request;
use Dingo\Api\Routing\Router;


$api = app(Router::class);


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

// Totalsend delivary report
Route::get('/totalsend-delivery-report', 'Api\\TotalsendDeliveryReport@store');

// SMS MO routing
Route::get('/sms/mo', 'Api\\SmsMoRouting@sms_mo');



/** @var Router $api */

//

$api->version(['v1'], function (Router $api) {

    /*
    |--------------------------------------------------------------------------
    | Authentication routes http://{domain}/api/auth/
    |--------------------------------------------------------------------------
    | These routes are used authentication and will work with V1 and V2
    */
    $api->group(['prefix' => 'auth'], function(Router $api) {

        // Authentication Routes
        $api->post('login', 'App\\Http\\Api\\Auth\\LoginController@login'); // Logs a user in
        $api->post('recovery', 'App\\Http\\Api\\Auth\\ForgotPasswordController@sendResetEmail'); // Sends the user an email to reset their password
        $api->post('reset', 'App\\Http\\Api\\Auth\\ResetPasswordController@resetPassword'); // Resets a users password
    });


    /*
       |--------------------------------------------------------------------------
       | Version 1 http://{domain}/api/v1/
       |--------------------------------------------------------------------------
       | These routes are used for the second version of the APP
       | These will used with the ondash implementation
       */
    $api->group(['middleware' => ['api.auth'], 'prefix' => 'v1', 'namespace' => 'App\\Http\\Api\\V1\\Controllers'], function (Router $api) {

        // Binds
        $api->get('binds', 'BindsController@index'); // Get all binds
        $api->post('binds', 'BindsController@store'); // Create a bind
        $api->get('binds/{id}', 'BindsController@show'); // Get single bind
        $api->post('binds/{id}', 'BindsController@update'); // Update a bind
        $api->post('binds/delete/{id}', 'BindsController@destroy'); // Destroy a bind
        $api->post('binds/recover/{id}', 'BindsController@restore'); // Restore a bind

        // Campaigns
        $api->get('campaigns/scheduled', 'CampaignsController@indexByScheduled'); // Get all campaigns by status scheduled
        $api->get('campaigns/executed', 'CampaignsController@indexByExecuted'); // Get all campaigns by status executed
        $api->get('campaigns/cancelled', 'CampaignsController@indexByCancelled'); // Get all campaigns by status cancelled
        $api->get('campaigns/{id}', 'CampaignsController@show'); // Get single campaign
        $api->post('campaigns', 'CampaignsController@store'); // Create a new campaign
        $api->post('campaigns/{id}', 'CampaignsController@update'); // Update a campaign

        // Campaign Types
        $api->get('campaign-types', 'CampaignTypesController@index'); // Get all campaign types

        // Delimiters
        $api->get('delimiters', 'DelimitersController@index'); // Get all delimiters

        // Notifications
        $api->post('notifications/{user_id}/{notification_id}', 'NotificationsController@markAsRead');

        // Roles ToDo - JPR - Swagger update
        $api->get('roles', 'RolesController@index'); // Get all roles
        $api->post('roles', 'RolesController@store'); // Create a role
        $api->get('roles/{id}', 'RolesController@show'); // Get single role
        $api->post('roles/{id}', 'RolesController@update'); // Update a role
        $api->post('roles/{id}/assign_permission/{permission_id}', 'RolesController@assignPermissionToRole'); // Assign Permission to role
        $api->post('roles/{id}/assign/{user_id}', 'RolesController@assignRoleToUser'); // Assign a role to a user
        $api->post('roles/{id}/remove/{user_id}', 'RolesController@removeRoleFromUser'); // Revoke a role from a user

        // Permissions ToDo - JPR - Swagger update
        $api->get('permissions', 'PermissionsController@index'); // Get all permissions
        $api->post('permissions', 'PermissionsController@store'); // Create a permission
        $api->get('permissions/{id}', 'PermissionsController@show'); // Get single permission
        $api->post('permissions/{id}', 'PermissionsController@update'); // Update a permission

        // Segments
        $api->get('segments', 'SegmentsController@index'); // Get all segments
        $api->post('segments', 'SegmentsController@store'); // Create a segment
        $api->get('segments/{id}', 'SegmentsController@show'); // Get single segment
        $api->post('segments/{id}', 'SegmentsController@update'); // Update a segment
        $api->post('segments/delete/{id}', 'SegmentsController@destroy'); // Destroy a segment
        $api->post('segments/recover/{id}', 'SegmentsController@restore'); // Restore a segment

        // Uploads
        $api->post('upload/csv', 'UploadsController@storeCSV'); // Create a segment

        // User Settings
        $api->post('user/change_password', 'UserSettingsController@changeCurrentUserPassword'); // Change current user password
        $api->post('user/update', 'UserSettingsController@updateCurrentUser'); // Update current user details
        $api->get('user', 'UserSettingsController@showCurrentUser'); // Get current user profile

        // Users
        $api->get('users', 'UsersController@index'); // Get all users
        $api->post('users', 'UsersController@store'); // Create a user
        $api->get('users/{id}', 'UsersController@show'); // Get single user
        $api->post('users/{id}', 'UsersController@update'); // Update a user
        $api->post('users/{id}/delete', 'UsersController@destroy'); // Delete profile
        $api->post('users/{id}/recover', 'UsersController@restore'); // Recover deleted profile





    });
    
});