<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// General CRUD routes for editing, creating and storing data
Route::resource('campaigns', 'CampaignController');
Route::resource('segments', 'SegmentController');
Route::resource('binds', 'BindController');
Route::resource('lists', 'ListController');

Route::get('fire/{status}/{message}', function ($status, $message) {
    // this fires the event
    $user = App\Models\User::find(1);
    $user->notify(new \App\Notifications\CampaignUpload($status, $message));
    return "event fired";
});

Route::get('test', function () {
    return view('test');
});